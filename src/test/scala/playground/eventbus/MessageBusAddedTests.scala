package playground.eventbus

import java.util.UUID

import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import timeseries.engine.common.data.DoubleIndex
import timeseries.engine.module.{DataPoint, TextValue}
import org.scalatest.{BeforeAndAfterAll, MustMatchers, WordSpecLike}

import scala.concurrent.duration._

class MessageBusAddedTests extends TestKit(ActorSystem("EventStreamTest"))
  with WordSpecLike with BeforeAndAfterAll with MustMatchers {

  override def afterAll(): Unit = {
    system.terminate()
  }

  "EventStream" must {
    "distribute messages" in {
      val sub1 = TestProbe()
      val sub2 = TestProbe()

      system.eventStream.subscribe(
        sub1.ref,
        classOf[MessageAdded])

      system.eventStream.subscribe(
        sub2.ref,
        classOf[MessageAdded])

      val correlationId = UUID.randomUUID()
      val id = UUID.randomUUID()
      val msg = MessageAdded(correlationId, Message(id, new DataPoint(DoubleIndex(5.1), TextValue("Hello World"))))
      system.eventStream.publish(msg)

      sub1.expectMsg(msg)
      sub2.expectMsg(msg)

    }
    "Ignore other messages" in {
      val giftModule = TestProbe()

      system.eventStream.subscribe(
        giftModule.ref,
        classOf[MessageAdded])
      val msg = "should ignore"
      system.eventStream.publish(msg)
      giftModule.expectNoMessage(3 seconds)

    }
    "unscribe messages" in {

      val sub1 = TestProbe()
      val sub2 = TestProbe()

      system.eventStream.subscribe(
        sub1.ref,
        classOf[MessageAdded])
      system.eventStream.subscribe(
        sub2.ref,
        classOf[MessageAdded])

      val correlationId = UUID.randomUUID()
      val id = UUID.randomUUID()
      val msg = MessageAdded(correlationId, Message(id, new DataPoint(DoubleIndex(5.1), TextValue("Hello World"))))
      system.eventStream.publish(msg)

      sub1.expectMsg(msg)
      sub2.expectMsg(msg)

      system.eventStream.unsubscribe(sub1.ref)

      system.eventStream.publish(msg)
      sub2.expectMsg(msg)
      sub1.expectNoMessage(3 seconds)

    }
  }
//  "MyEventBus" must {
//    "deliver all messages" in {
//      val bus = new MyEventBus
//      val systemLog = TestProbe()
//      bus.subscribe(systemLog.ref)
//      val msg = new Order("me", "Akka in Action", 3)
//      bus.publish(msg)
//      systemLog.expectMsg(msg)
//
//      bus.publish("test")
//      systemLog.expectMsg("test")
//
//    }
//  }
  "MessageAddedBus" must {
    "deliver MessageAdded messages" in {

      val bus = new MessageAddedBus
      val correlationId1 = UUID.randomUUID()
      val sub1 = TestProbe()
      bus.subscribe(sub1.ref, correlationId1)

      val correlationId2 = UUID.randomUUID()
      val sub2 = TestProbe()
      bus.subscribe(sub2.ref, correlationId2)



      val id1 = UUID.randomUUID()
      val msg1 = MessageAdded(correlationId1, Message(id1, new DataPoint(DoubleIndex(5.1), TextValue("Hello World"))))
      bus.publish(msg1)
      sub1.expectMsg(msg1)
      sub2.expectNoMessage(3 seconds)


      val id2 = UUID.randomUUID()
      val msg2 = MessageAdded(correlationId2, Message(id2, new DataPoint(DoubleIndex(5.1), TextValue("Hello World"))))
      bus.publish(msg2)
      sub1.expectNoMessage(3 seconds)
      sub2.expectMsg(msg2)

    }
    "deliver order messages when multiple subscriber" in {
      val bus = new MessageAddedBus
      val listener = TestProbe()

      val correlationId = UUID.randomUUID()

      bus.subscribe(listener.ref, correlationId)
      bus.subscribe(listener.ref, correlationId)

      val id1 = UUID.randomUUID()
      val msg1 = MessageAdded(correlationId, Message(id1, new DataPoint(DoubleIndex(5.1), TextValue("Hello World"))))
      bus.publish(msg1)
      listener.expectMsg(msg1)

      val id2 = UUID.randomUUID()
      val msg2 = MessageAdded(correlationId, Message(id2, new DataPoint(DoubleIndex(5.1), TextValue("Hello World"))))
      bus.publish(msg2)
      listener.expectMsg(msg2)
    }
  }

  "MessageAddedBus Stress Tests" must {
    val bus = new MessageAddedBus
    val testPropes = scala.collection.mutable.Map[UUID, TestProbe]()
    val correlationIds = scala.collection.mutable.ListBuffer.empty[UUID]
    val range = 1 to 100
    val msgPerCorrelationId = 1000000
    val activeCorrelationIdCount = 3
    "init correlation id list" in {
      range.foreach(_ => correlationIds += UUID.randomUUID)
    }

    "prepare subscribers" in {
      for(cId <- correlationIds){
        val testPrope = TestProbe()
        testPropes += (cId -> testPrope)
        bus.subscribe(testPrope.ref, cId)
      }
    }

//    lazy val n = util.Random.nextInt(correlationIds.size)
//    lazy val correlationId = correlationIds.drop(n).head
//    lazy val testPrope = testPropes(correlationId)
    val messages = scala.collection.mutable.ListMap[UUID, Array[MessageAdded]]()//scala.collection.mutable.ListBuffer.empty[MessageAdded]
    "prepare 10M messages. 1M for each first 10 correlation Ids" in {
      val id = UUID.randomUUID()
      //var count = 0
      for(ci <- correlationIds.take(activeCorrelationIdCount)){
        println(ci)
        val listBuffer = scala.collection.mutable.ListBuffer.empty[MessageAdded]


        for(_ <- 1 to msgPerCorrelationId){
          val msg: MessageAdded = MessageAdded(ci, Message(id, new DataPoint(DoubleIndex(5.1), TextValue("Hello World"))))
          listBuffer += msg
          //count = count + 1; println(s"$count: $msg")
        }
        messages += (ci -> listBuffer.toArray)
      }

      //println(messages.size)

      //messages.foreach(x => {count = count + 1; println(s"$count: $x")})
    }

    "0 do nothing with the list" in {
      val ci = correlationIds.head
      val msgs = messages(ci)
      //println(s"publishing ${msgs.size} messages for correlation id $ci")
      var count = 0
      msgs.take(msgPerCorrelationId).foreach(x => count = count + 1)
      println(count)
    }
    "1. publish 1M" in {
      //println(1)
      for(i <- 1 to 10){
        val ci = correlationIds.head
        val msgs = messages(ci)
        println(s"publishing ${msgs.size} messages for correlation id $ci")
        msgs.take(msgPerCorrelationId).foreach(bus.publish)
      }

    }
    "2. recieve 1M" in {
      //println(2)
      for(i <- 1 to 10){
        val ci = correlationIds.head
        println(s"recieving $msgPerCorrelationId for corrlationid $ci")
        val testPrope = testPropes(ci)
        val typ = classOf[MessageAdded]
        //testPrope.expectMsgType(typ)
        (1 to msgPerCorrelationId).foreach(_ => testPrope.expectMsgClass(typ))
      }

    }

//    "3. prepare additional 4M messages with a random correlation id not in the set" in {
//      val id = UUID.randomUUID()
//      val randomCorrelationId = UUID.randomUUID()
//      for(_ <- 1 to 4000000){
//        val msg: MessageAdded = MessageAdded(randomCorrelationId, Message(id, new DataPoint(DoubleIndex(5.1), TextValue("Hello World"))))
//        messages += msg
//      }
//    }
//    "3. publish 5M" in {
//      //println(3)
//      messages.foreach(bus.publish)
//    }
//    "4. recieve 1M that belongs to the test Prope" in {
//      //println(4)
//      val ci = correlationIds.head
//      //println(ci)
//      val testPrope = testPropes(ci)
//      (1 to 1000000).foreach{x => println(x); testPrope.expectMsgClass(classOf[MessageAdded])}
//    }
  }
}

