package playground.eventbus

import java.util.UUID

import timeseries.engine.module.DataPoint

case class MessageAdded(correlationId : UUID, messageAdded : Message)
case class Message(id : UUID, datapoint : DataPoint)
