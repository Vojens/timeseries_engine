package playground.eventbus

import java.util.UUID

import akka.actor.ActorRef
import akka.event.{ActorEventBus, EventBus, LookupClassification}

class MessageAddedBus extends EventBus with LookupClassification with ActorEventBus {
  override type Event = MessageAdded
  override type Classifier = UUID

  override protected def mapSize(): Int = 2

  override protected def classify(event: MessageAddedBus#Event): MessageAddedBus#Classifier = {
    event.correlationId
  }

  override protected def publish(event: MessageAddedBus#Event, subscriber: ActorRef): Unit = {
    subscriber ! event
  }
}
