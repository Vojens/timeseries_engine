import java.util.UUID

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import timeseries.engine.common.data._
import timeseries.engine.common.msg.{ChannelDeleted, ChannelMetadataAdded, ChannelMetadataNotExists}
import timeseries.engine.services.MetadataService
import org.scalatest.WordSpecLike

import scala.concurrent.duration._
import timeseries.engine.common.msg.DeletedChannelMetaData

class ChannalMetadataSupervisorFunctionalityTests extends TestKit(ActorSystem("timeseries"))
  with WordSpecLike
  with ImplicitSender
  with StopSystemAfterAll {

  val channelMetadata = ChannelMetadata(
    UUID.randomUUID(),
    Type.Timestamp,
    ExternalType.Double,
    "annotation",
    "uri",
    DataType.Long,
    DataSetType.Raw,
    "customDataType",
    "mnemonic",
    "uom",
    Stream[Int](1,2,3,4),
    Map[String,String]("a" -> "b", "c" -> "d"),
    "description",
    UUID.randomUUID(),
    Stream[UUID](UUID.randomUUID(), UUID.randomUUID()),
    Map[String,String]("bkt" -> "info"),
    IndexType.Int,
    "indexUOM",
    "indexDTM",
    IndexDirection.Increasing,
    "indexMnemonic",
    "indexDescription",
    "indexUri",
    org.joda.time.DateTime.now().minusMinutes(5),
    org.joda.time.DateTime.now().minusDays(3),
    org.joda.time.DateTime.now().minusMinutes(10),
    Status.Inactive,
    null,
    null,
    false
  )
  "channel metadata" must {
    "be added successfully into the database" in {

      MetadataService.AddMetadata(channelMetadata, testActor)
      expectMsgClass(classOf[ChannelMetadataAdded])
    }

    "be able te fetch existing metadata" in {
      MetadataService.GetMetadata(channelMetadata.id, testActor)
      expectMsgClass(5000 millis, classOf[ChannelMetadata])
    }

    "must get non exists when a channel metadata doesn't exist" in {
      val chId = UUID.randomUUID()
      MetadataService.GetMetadata(chId, testActor)
      expectMsgClass(5000 millis, classOf[ChannelMetadataNotExists])
    }

    "be able te fetch metadata when creation Time ==  modification Time" in {
      val now = org.joda.time.DateTime.now()
      val cm = channelMetadata.copy(id = UUID.randomUUID(),
        modificationTime = now,
        creationTime = now,
        changeTime = now)

      MetadataService.AddMetadata(cm, testActor)
      expectMsgClass(6000 millis, classOf[ChannelMetadataAdded])

      MetadataService.GetMetadata(cm.id, testActor)
      expectMsgClass(6000 millis, classOf[ChannelMetadata])

    }

    "be able to delete channel metadata" in {
      val cm = channelMetadata.copy(id = UUID.randomUUID())
      
      MetadataService.AddMetadata(cm, testActor)
      expectMsgClass(6000 millis, classOf[ChannelMetadataAdded])

      MetadataService.GetMetadata(cm.id, testActor)
      expectMsgClass(6000 millis, classOf[ChannelMetadata])

      MetadataService.deleteMetadata(cm.id, testActor)
      expectMsgClass(6000 millis, classOf[ChannelDeleted])

      MetadataService.GetMetadata(cm.id, testActor)
      expectMsgClass(6000 millis, classOf[DeletedChannelMetaData])
    }
  }

  "channel metadata status" must {
    "must be able to change status after timeout" in {
      val chId = UUID.randomUUID()
      MetadataService.AddMetadata(channelMetadata.copy(id = chId, status = Status.Active), testActor)
      expectMsgClass(5000 millis, classOf[ChannelMetadataAdded])
      MetadataService.GetMetadata(chId, testActor)
      expectMsgClass(5000 millis, classOf[ChannelMetadata])
      Thread.sleep(31000)
      MetadataService.GetMetadata(chId, testActor)
      expectMsg(5000 millis, channelMetadata.copy(id = chId, status = Status.Inactive))
    }
  }
}
