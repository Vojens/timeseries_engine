import java.util.UUID

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import timeseries.engine.common.data._
import timeseries.engine.common.msg.ChannelMetadataAdded
import timeseries.engine.services.MetadataService
import org.scalatest.WordSpecLike

import scala.collection.mutable.ArrayBuffer

class ChannalMetadataSupervisorStressTests extends TestKit(ActorSystem("timeseries"))
  with WordSpecLike
  with ImplicitSender
  with StopSystemAfterAll {

  val channelMetadata = ChannelMetadata(
    UUID.randomUUID(),
    Type.Timestamp,
    ExternalType.Double,
    "annotation",
    "uri",
    DataType.Long,
    DataSetType.Raw,
    "customDataType",
    "mnemonic",
    "uom",
    Stream[Int](1,2,3,4),
    Map[String,String]("a" -> "b", "c" -> "d"),
    "description",
    UUID.randomUUID(),
    Stream[UUID](UUID.randomUUID(), UUID.randomUUID()),
    Map[String,String]("bkt" -> "info"),
    IndexType.Int,
    "indexUOM",
    "indexDTM",
    IndexDirection.Increasing,
    "indexMnemonic",
    "indexDescription",
    "indexUri",
    org.joda.time.DateTime.now().minusMinutes(5),
    org.joda.time.DateTime.now().minusDays(3),
    org.joda.time.DateTime.now().minusMinutes(10),
    Status.Inactive,
    null,
    null,
    false
  )
  "channel metadata" must {


    "be able te fetch existing metadata multiple times concurrently" in {
      val range = 1 to 1000000
      for(i <- range){
        MetadataService.GetMetadata(channelMetadata.id, testActor)
      }
      for(i <- range){
        expectMsgClass(classOf[ChannelMetadata])
      }
    }

    "be able te fetch existing metadata multiple times sequencially" in {
      val range = 1 to 1000000
      for(i <- range){
        MetadataService.GetMetadata(channelMetadata.id, testActor)
        expectMsgClass(classOf[ChannelMetadata])
      }
    }

    "be able to insert lots of metadata concurrently" in {
      val range = 1 to 100000
      val arrayBuffer : scala.collection.mutable.ArrayBuffer[ChannelMetadata] = ArrayBuffer()
      for(i <- range){
        val m = channelMetadata.copy(id = UUID.randomUUID())
        arrayBuffer += m
        MetadataService.AddMetadata(m, testActor)
        //val cma = ChannelMetadataAdded(m)
        //println(cma)
        //expectMsg(cma)
      }
      arrayBuffer.foreach(m => {
        //        val cma = ChannelMetadataAdded(m)
        //        println(cma)
        //        expectMsg(cma)
        expectMsgClass(classOf[ChannelMetadataAdded])
      })
      //      list.foreach(m => {
      //        expectMsg(ChannelMetadataAdded(m))
      //      })
    }
  }
}
