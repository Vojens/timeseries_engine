import java.util.UUID

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import timeseries.engine.common.data._
import timeseries.engine.common.msg.ChannelMetadataAdded
import timeseries.engine.services.MetadataService
import org.scalatest.WordSpecLike

import scala.collection.mutable.ArrayBuffer

class ChannalMetadataSupervisorBenchmarkTests extends TestKit(ActorSystem("timeseries"))
  with WordSpecLike
  with ImplicitSender
  with StopSystemAfterAll {

  val channelMetadata = ChannelMetadata(
    UUID.randomUUID(),
    Type.Timestamp,
    ExternalType.Double,
    "annotation",
    "uri",
    DataType.Long,
    DataSetType.Raw,
    "customDataType",
    "mnemonic",
    "uom",
    Stream[Int](1,2,3,4),
    Map[String,String]("a" -> "b", "c" -> "d"),
    "description",
    UUID.randomUUID(),
    Stream[UUID](UUID.randomUUID(), UUID.randomUUID()),
    Map[String,String]("bkt" -> "info"),
    IndexType.Int,
    "indexUOM",
    "indexDTM",
    IndexDirection.Increasing,
    "indexMnemonic",
    "indexDescription",
    "indexUri",
    org.joda.time.DateTime.now().minusMinutes(5),
    org.joda.time.DateTime.now().minusDays(3),
    org.joda.time.DateTime.now().minusMinutes(10),
    Status.Inactive,
    null,
    null,
    false
  )

  val arrayBuffer : scala.collection.mutable.ArrayBuffer[ChannelMetadata] = ArrayBuffer()
  val range = 1 to 100000
  "channel metadata" must {

    "prepare list" in {
      for (_ <- range) {
        val m = channelMetadata.copy(id = UUID.randomUUID())
        arrayBuffer += m
      }
    }

    "be able to insert lots of metadata concurrently" in {
      for (m <- arrayBuffer) {
        MetadataService.AddMetadata(m, testActor)
      }
      arrayBuffer.foreach(m => {
        expectMsgClass(classOf[ChannelMetadataAdded])
      })
    }

    "allow compaction for 30 seconds" in {
      Thread.sleep(30000)
    }

    "fetch existing metadatas" in {
      arrayBuffer.foreach(m => {
        MetadataService.GetMetadata(m.id, testActor)
      })
      arrayBuffer.foreach(_ => {
        expectMsgClass(classOf[ChannelMetadata])
      })
    }
  }

  "channel metadata where crTime == mTime" must{
    val now = org.joda.time.DateTime.now()
    val mt = channelMetadata.copy(id = UUID.randomUUID(),
      creationTime = now,
      modificationTime = now,
      changeTime = now)

    "add metadata where crTime == mTime" in {
      MetadataService.AddMetadata(mt, testActor)
      expectMsgClass(classOf[ChannelMetadataAdded])
    }

    "warm up with 1k" in {
      val range = 1 to 1000
      for(i <- range){
        MetadataService.GetMetadata(mt.id, testActor)
      }
      for(i <- range){
        expectMsgClass(classOf[ChannelMetadata])
      }
    }

    "fetch metadata 100k times" in {
      val range = 1 to 100000
      for(i <- range){
        MetadataService.GetMetadata(mt.id, testActor)
      }
      for(i <- range){
        expectMsgClass(classOf[ChannelMetadata])
      }

    }
  }

  "channel metadata where crTime != mTime" must{
    val now = org.joda.time.DateTime.now()
    val mt = channelMetadata.copy(id = UUID.randomUUID(),
      creationTime = now,
      modificationTime = now.plusHours(3),
      changeTime = now)

    "add metadata where crTime != mTime" in {
      MetadataService.AddMetadata(mt, testActor)
      expectMsgClass(classOf[ChannelMetadataAdded])
    }

    "warm up with 1k" in {
      val range = 1 to 1000
      for (i <- range) {
        MetadataService.GetMetadata(mt.id, testActor)
      }
      for (i <- range) {
        expectMsgClass(classOf[ChannelMetadata])
      }
    }

    "fetch metadata 100k times" in {
      val range = 1 to 100000
      for(i <- range){
        MetadataService.GetMetadata(mt.id, testActor)
      }
      for(i <- range){
        expectMsgClass(classOf[ChannelMetadata])
      }
    }
  }
}

