timeseries{
  repository{
    cql{
      keyspace{
        core = "timeseries"
        journal = "timeseries-journal"
      }
      timeseries.engine.store.cassandra.repository.statement{
        StaticChannelMetadataStatements{
          keyspace = ${timeseries.repository.cql.keyspace.core}
          table = "static_channel_metadata"
          selectAllMetadata = {
            cql : select "*" from ${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.StaticChannelMetadataStatements.keyspace}.${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.StaticChannelMetadataStatements.table}
            consistency-level = ONE
            enable-tracing : true
            idempotent : false
            read-timeout-in-millis = 6000
            retry-policy { logging-retry-policy{policy{downgrading-consistency-retry-policy{}}}}
            serial-consistency-level = LOCAL_SERIAL
          }
          selectMetadata = {
            cql : select "*" from ${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.StaticChannelMetadataStatements.keyspace}.${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.StaticChannelMetadataStatements.table} where id "= ?"
          }
          insertMetadata = {
            cql : insert into ${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.StaticChannelMetadataStatements.keyspace}.${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.StaticChannelMetadataStatements.table} "(id, typ, exttyp, annt,uri,dattyp,datsettyp,custdattyp,mnem,uom,addlidx,extn,descr,timlinid,src,idxtyp,idxuom,idxdtm,idxdircn,idxmnem,idxdescr,idxuri,ctime,ctime_d,ctime_t,crtime,crtime_d,crtime_t,bktinfo,isdel) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"
          }
          deleteMetadata = {
          	cql : delete from ${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.StaticChannelMetadataStatements.keyspace}.${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.StaticChannelMetadataStatements.table} where id "= ?"
          }
          deleteMetaDataByDelFlag = {
          	cql : update ${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.StaticChannelMetadataStatements.keyspace}.${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.StaticChannelMetadataStatements.table} set isdel "= ?" where id "= ?"
          }
        }
        DynamicChannelMetadataStatements{
          keyspace = ${timeseries.repository.cql.keyspace.core}
          table = "dynamic_channel_metadata"
          selectAllDynamicMetadata = {
        	cql : select "id,subscript_i,subscript_j,element,tags" from ${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.DynamicChannelMetadataStatements.keyspace}.${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.DynamicChannelMetadataStatements.table}
          }
          selectDynamicMetadata = {
        	cql : select "id,subscript_i,subscript_j,element,tags" from ${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.DynamicChannelMetadataStatements.keyspace}.${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.DynamicChannelMetadataStatements.table} where id "=?" and subscript_i "=?" and subscript_j "=?"
          }
          selectDynamicMetadataList = {
            cql : select "id,subscript_i,subscript_j,element,tags" from ${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.DynamicChannelMetadataStatements.keyspace}.${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.DynamicChannelMetadataStatements.table} where id "=?" and subscript_i "=?"
          }
          insertDynamicMetadata = {
            cql : insert into ${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.DynamicChannelMetadataStatements.keyspace}.${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.DynamicChannelMetadataStatements.table} "(id,subscript_i,subscript_j,element,tags) values(?,?,?,?,?)"
          }
        }
        ToBeDeletedChannelStatements{
          keyspace = ${timeseries.repository.cql.keyspace.core}
          table = "to_be_deleted_channel"
          insertToBeDeletedChannel = {
            cql : insert into ${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.ToBeDeletedChannelStatements.keyspace}.${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.ToBeDeletedChannelStatements.table} "(id) values(?)"
          }
        }
        DoubleChannelStatements{
          keyspace = ${timeseries.repository.cql.keyspace.core}
          table = "double_channel"
          insertDataPoint = {
            cql : insert into ${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.DoubleChannelStatements.keyspace}.${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.DoubleChannelStatements.table} "(chid,timlinid,bktid,idx,ascii,bi,blb,d,dec,dt,i,inet,si,ti,tim,ts,txt,uid,vi) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
          }
        }
        TimestampChannelStatements{
          keyspace = ${timeseries.repository.cql.keyspace.core}
          table = "timestamp_channel"
          insertDataPoint = {
            cql : insert into ${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.TimestampChannelStatements.keyspace}.${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.TimestampChannelStatements.table} "(chid,timlinid,bktid,idx,ascii,bi,blb,d,dec,dt,i,inet,si,ti,tim,ts,txt,uid,vi) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
          }
        }
        ChannelBucketTimelineStatements{
          keyspace = ${timeseries.repository.cql.keyspace.core}
          table = "channel_bucket_timeline"
          insertChannelBucket = {
            cql : insert into ${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.ChannelBucketTimelineStatements.keyspace}.${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.ChannelBucketTimelineStatements.table} "(chid,timlinid,bkt,bktid) values(?,?,?,?)"
          }
          readChannelBuckets = {
            cql : select "chid,timlinid,bkt,bktid" from ${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.ChannelBucketTimelineStatements.keyspace}.${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.ChannelBucketTimelineStatements.table} "WHERE chid = ? AND timlinid = ?"
          }
        }
        DefaultBucketInfoStatements{
          keyspace = ${timeseries.repository.cql.keyspace.core}
          table = "default_bucket_info"
          insertDefaultBucketInfo = {
            cql : insert into ${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.DefaultBucketInfoStatements.keyspace}.${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.DefaultBucketInfoStatements.table} "(typ,dattyp,bktinfo) values(?,?,?)"
          }
          selectDefaultBucketInfo = {
            cql : select "typ,dattyp,bktinfo" from ${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.DefaultBucketInfoStatements.keyspace}.${timeseries.repository.cql.timeseries.engine.store.cassandra.repository.statement.DefaultBucketInfoStatements.table} "WHERE typ = ? AND dattyp = ?"
          }
        }
      }
    }
  }
}