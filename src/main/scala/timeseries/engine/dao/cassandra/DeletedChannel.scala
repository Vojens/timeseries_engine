package timeseries.engine.dao.cassandra

case class DeletedChannel(
                           id:java.util.UUID,
                           delReason:java.lang.String,
                           deletionTime:java.util.Date
)
