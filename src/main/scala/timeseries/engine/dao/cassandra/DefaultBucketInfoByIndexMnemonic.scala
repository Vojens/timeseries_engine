package timeseries.engine.dao.cassandra

case class DefaultBucketInfoByIndexMnemonic(
                                        indexMnemonic:String,
                                        `type`:Int,
                                        dataType:Int,
                                        bucketInfo:java.util.Map[String,String]
)