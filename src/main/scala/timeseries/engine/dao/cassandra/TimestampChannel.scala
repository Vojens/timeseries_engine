package timeseries.engine.dao.cassandra

/**
 * time channel schema for poc
 */
case class TimestampChannel(
                             channelId:        java.util.UUID,     //chid        UUID
                             timelineId:       java.util.UUID,     //timlinid  	UUID
                             bucketId:         java.util.UUID,     //bktid  		  UUID
                             index:            java.util.Date,     //idx 		    double

                             asciiValue:       Option[java.lang.String],     //ascii ASCII
                             longValue:        Option[java.lang.Long],       //bi 	  bigint
                             blobValue:        Option[java.nio.ByteBuffer],  //blb 	BLOB
                             doubleValue:      Option[java.lang.Double],     //d	    double
                             decimalValue:     Option[java.math.BigDecimal], //dec	  DECIMAL
                             dateValue:        Option[com.datastax.driver.core.LocalDate],       //dt	  DATE
                             intValue:         Option[java.lang.Integer],    //i	    int
                             iNetAddressValue: Option[java.net.InetAddress], //inet  inet
                             smallIntValue:    Option[java.lang.Short],      //si	  smallint
                             tinyIntValue:     Option[java.lang.Byte],       //ti	  TINYINT
                             timeValue:        Option[java.lang.Long],       //tim	  TIME
                             timestampValue:   Option[java.util.Date],       //ts	  TIMESTAMP
                             textValue:        Option[java.lang.String],     //txt 	text
                             uuidValue:        Option[java.util.UUID],       //vi	  varint
                             varIntValue:      Option[java.math.BigInteger], //uid	  UUID

)