package timeseries.engine.dao.cassandra

case class ToBeDeletedChannel(
                               id:java.util.UUID
)