package timeseries.engine.dao.cassandra

case class ChannelBucketTimeline(
                                  channelId:java.util.UUID,
                                  timelineId:java.util.UUID,
                                  bucket:java.nio.ByteBuffer,
                                  bucketId:Option[java.util.UUID]
)