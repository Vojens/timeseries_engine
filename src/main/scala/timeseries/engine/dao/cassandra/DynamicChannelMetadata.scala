package timeseries.engine.dao.cassandra

case class DynamicChannelMetadata(
                                   id:java.util.UUID,
                                   subscript_i:java.lang.Byte,
                                   subscript_j:java.lang.Byte,
                                   element:Option[java.nio.ByteBuffer],
                                   tags:Option[java.util.Map[java.lang.String,java.lang.String]]
)