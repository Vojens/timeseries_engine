package timeseries.engine.dao.cassandra

import timeseries.engine.store.cassandra.mapping.Cql3DataTypes
case class DefaultBucketInfo(
                              `type` : java.lang.Byte,
                              dataType : java.lang.Byte,
                              bucketInfo:Option[java.util.Map[java.lang.String,java.lang.String]]
) extends Cql3DataTypes