package timeseries.engine.main

import akka.actor.Props
import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.Http
import akka.stream.{ActorMaterializer, Materializer}
import com.datastax.driver.core.Session
import timeseries.engine.store.cassandra.connectionprovider.{CassandraConfig, CassandraSession}
import com.typesafe.config.ConfigFactory
import scala.concurrent.ExecutionContextExecutor

object Main extends App{
  println("Hello, World! From Main")
  val system = ActorSystemInitilizer.system
  val actor = system.actorOf(Props[HelloTimeseries])
  val config = system.settings.config


  implicit val actorSystem: ActorSystem=ActorSystemInitilizer.system
  implicit def executor: ExecutionContextExecutor = system.dispatcher
  implicit val materializer: Materializer = ActorMaterializer()
  val config = ConfigFactory.load()
  val logger: LoggingAdapter=Logging(system, getClass)
  logger.info("Initilizing Service...")
  logger.info("Setting up database connection...")
  logger.info("Registering endpoints...")
  Http().bindAndHandle(RestService.routes, config.getString("http.interface"), config.getInt("http.port"))
  logger.info("Service is Up")

}

object ActorSystemInitilizer{
  val system=ActorSystem("Timeseries")
  val config=ConfigFactory.load("cassandra")
  val cassandraConfig=new CassandraConfig(config.getConfig("timeseries.store.cassandra"))
  val session:Session=CassandraSession.createSession(cassandraConfig)
}
