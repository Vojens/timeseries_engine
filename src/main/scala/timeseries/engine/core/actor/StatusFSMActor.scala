package timeseries.engine.core.actor

import java.util.UUID

import akka.actor.{Actor, ActorRef, FSM, Props}
import timeseries.engine.common.data.Status.Status
import timeseries.engine.common.data._
import timeseries.engine.core.actor.StatusFSMActor.{CheckStatus, GetStatusChannelMetadata}
import timeseries.engine.store.cassandra.repository.`trait`.{DynamicChannelMetadataRepository, PrimitiveDynamicChannelMetadata, RichDynamicChannelMetadataRepository}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object StatusFSMActor{
  def props(id:UUID, dynamicChannelMetadataRepository : DynamicChannelMetadataRepository) = Props(new StatusFSMActor(id, dynamicChannelMetadataRepository))
  def name(id:UUID) = s"status-${id.toString}"

  case class AddStatusChannelMetadata(metadata:PrimitiveDynamicChannelMetadata, replyTo:ActorRef)
  case class GetStatusChannelMetadata(id:UUID, replyTo : ActorRef)
  private object CheckStatus

}

class StatusFSMActor(val id:UUID,  dynamicChannelMetadataRepository : DynamicChannelMetadataRepository) extends Actor with FSM[StatusFSM, StatusFSMData]{
  startWith(UndefinedFSM, new StatusFSMData(new org.joda.time.DateTime(Long.MinValue)))
  val rdcmt = dynamicChannelMetadataRepository.asInstanceOf[RichDynamicChannelMetadataRepository]
  //val r = dynamicChannelMetadataRepository with RichDynamicChannelMetadataRepository
  var mTime = org.joda.time.DateTime.now()

  final val statusTimeoutInMillis : Long = 30000
  private def determineStatus(pdcm: PrimitiveDynamicChannelMetadata) : Status = {
    if(pdcm.mTime.isDefined && pdcm.status.isDefined){
      val mTime = new org.joda.time.DateTime(pdcm.mTime.get)
      val now = org.joda.time.DateTime.now()
      //val p = new Period(mTime,now)
      val oldStatus = Status(pdcm.status.get)
      if((now.getMillis-mTime.getMillis) > statusTimeoutInMillis && oldStatus == Status.Active){
        return Status.Inactive
      }
      return oldStatus
    }
    else if(pdcm.status.isDefined){
      return Status(pdcm.status.get)
    }
    return Status.Undefined
  }
  override def preStart(): Unit = {
    super.preStart()


    rdcmt.getStatusAndMTime(id)
      .map(x => {
        if(x.isDefined){
          val status = determineStatus(x.get)
          goto(StatusFSM.FromStatus(status))
        }
      })
  }

  when(UndefinedFSM) {
    case Event(addDynamicChannelMetadata:StatusFSMActor.AddStatusChannelMetadata, _: StatusFSMData) => {
      rdcmt.addStatusAndMTime(addDynamicChannelMetadata.metadata).onComplete{
        case Success(_) => addDynamicChannelMetadata.replyTo ! StatusChannelMetadataAdded(addDynamicChannelMetadata.metadata)
        case Failure(t) => addDynamicChannelMetadata.replyTo ! StatusChannelMetadataFailedToAdd(addDynamicChannelMetadata.metadata, t)
      }
      if(addDynamicChannelMetadata.metadata.mTime.isDefined){
        mTime = new org.joda.time.DateTime(addDynamicChannelMetadata.metadata.mTime.get)
      }

      //TODO when failure handle goto
      goto(StatusFSM.FromByte(addDynamicChannelMetadata.metadata.status.get))
    }
    case Event(getDynamicChannelMetadata: GetStatusChannelMetadata, _ : StatusFSMData) => {
      val primitiveDynamicChannelMetadata  = new PrimitiveDynamicChannelMetadata(
        getDynamicChannelMetadata.id,
        Some(Status.Undefined.id.byteValue()),
        Some(mTime.getMillis)
      )
      getDynamicChannelMetadata.replyTo ! primitiveDynamicChannelMetadata

      stay
    }
    case _ => stay
  }

  when(ActiveFSM) {
    case Event(CheckStatus, _: StatusFSMData) => {
      log.info(s"checking status for $id")
      val status = determineStatus(PrimitiveDynamicChannelMetadata(id, Some(Status.Active.id.byteValue()), Some(mTime.getMillis)))
      log.info(s"Status for $id was determined to be $status")
      if(status == Status.Active){
        context.system.scheduler.scheduleOnce(statusTimeoutInMillis millis, self, CheckStatus)
        stay
      }else{
        log.info(s"changing status of $id to $status")
        goto(StatusFSM.FromStatus(status))
      }
    }
    case Event(getDynamicChannelMetadata: GetStatusChannelMetadata, _ : StatusFSMData) => {
      val primitiveDynamicChannelMetadata  = new PrimitiveDynamicChannelMetadata(
        getDynamicChannelMetadata.id,
        Some(Status.Active.id.byteValue()),
        Some(mTime.getMillis)
      )
      getDynamicChannelMetadata.replyTo ! primitiveDynamicChannelMetadata

      stay
    }
    case _ => stay
  }

  when(ClosedFSM) {
    case Event(getDynamicChannelMetadata: GetStatusChannelMetadata, _ : StatusFSMData) => {
      val primitiveDynamicChannelMetadata  = new PrimitiveDynamicChannelMetadata(
        getDynamicChannelMetadata.id,
        Some(Status.Closed.id.byteValue()),
        Some(mTime.getMillis)
      )
      getDynamicChannelMetadata.replyTo ! primitiveDynamicChannelMetadata

      stay
    }
    case _ => stay
  }

  when(InactiveFSM) {
    case Event(getDynamicChannelMetadata: GetStatusChannelMetadata, _ : StatusFSMData) => {
      val primitiveDynamicChannelMetadata  = new PrimitiveDynamicChannelMetadata(
        getDynamicChannelMetadata.id,
        Some(Status.Inactive.id.byteValue()),
        Some(mTime.getMillis)
      )
      getDynamicChannelMetadata.replyTo ! primitiveDynamicChannelMetadata

      stay
    }
    case _ => stay
  }

  initialize

  onTransition {
    case _ -> ActiveFSM => {
      println("scheduling a FSM activity timeout")
      context.system.scheduler.scheduleOnce(statusTimeoutInMillis millis, self, CheckStatus)
    }
  }
}
