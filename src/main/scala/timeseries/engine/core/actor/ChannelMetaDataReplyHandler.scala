package timeseries.engine.core.actor

import akka.actor.Actor
import akka.actor.Props
import timeseries.engine.rest.JSONDataConverter
import spray.json._
import timeseries.engine.module.ResponseObj
import timeseries.engine.constants.Constants
import timeseries.engine.common.convert.ModuleConverters
import akka.actor.ActorLogging


object ChannelMetaDataReplyHandler{
  type MetadataExists = timeseries.engine.common.msg.ChannelMetadataExists
  type ChannelMetadataAdded = timeseries.engine.common.msg.ChannelMetadataAdded
  type ChannelMetadataFailedToAdd = timeseries.engine.common.msg.ChannelMetadataFailedToAdd
  type ChannelDeleted = timeseries.engine.common.msg.ChannelDeleted
  type ChannelDeletionFailed = timeseries.engine.common.msg.ChannelDeletionFailed
  type ChannelMetadataNotExists = timeseries.engine.common.msg.ChannelMetadataNotExists
  type ChannelMetadata = timeseries.engine.common.data.ChannelMetadata
  type DeletedChannelMetaData = timeseries.engine.common.msg.DeletedChannelMetaData
  type RequestValidationFailed = timeseries.engine.common.msg.RequestValidationFailed
}


class ChannelMetaDataReplyHandler(ctx:ImperativeRequestContext) extends Actor with JSONDataConverter with ActorLogging{
   def receive={
    case metaDataExits:ChannelMetaDataReplyHandler.MetadataExists=>{
      log.info(metaDataExits.toString)
      val respObj=ResponseObj(Constants.success,Constants.channelMetaDataAlreadyExists)
      ctx.complete(respObj.toJson.toString)
      context.stop(self)
    }
    case addedChannel:ChannelMetaDataReplyHandler.ChannelMetadataAdded=>{
      log.info("Recieved channel: "+ addedChannel)
      val respObj=ResponseObj(Constants.success,Constants.channelMetadataAdded)
      ctx.complete(respObj.toJson.toString)
      context.stop(self)
    }
    case failed:ChannelMetaDataReplyHandler.ChannelMetadataFailedToAdd=>{
      log.info(failed.toString)
    }
    case _:ChannelMetaDataReplyHandler.ChannelDeleted=>{
      log.info("channel deleted")
      val respObj=ResponseObj(Constants.success,Constants.channelMetadataDeleted)
      ctx.complete(respObj.toJson.toString)
      context.stop(self)
    }
    case _:ChannelMetaDataReplyHandler.ChannelDeletionFailed=>{
      log.info("channel deletion failed")
    }
    case channelmetadata:ChannelMetaDataReplyHandler.ChannelMetadata=>{
      log.info("fetched : "+channelmetadata)
      val responseObj=ModuleConverters.asChannelMetaDataResponseObj(channelmetadata)
      ctx.complete(responseObj.toJson.toString)
      context.stop(self)
    }
    case requestValidationFailed:ChannelMetaDataReplyHandler.RequestValidationFailed=>{
      val responseObj=ResponseObj(Constants.failed,requestValidationFailed.msg)
      ctx.complete(responseObj.toJson.toString)
      context.stop(self)
    }
    case _:ChannelMetaDataReplyHandler.ChannelMetadataNotExists=>{
      val respObj=ResponseObj(Constants.success,Constants.channelMetaDataNotExists)
      ctx.complete(respObj.toJson.toString)
      context.stop(self)
    }
    case _:ChannelMetaDataReplyHandler.DeletedChannelMetaData =>{
      val respObj=ResponseObj(Constants.success,Constants.channelDelted)
      ctx.complete(respObj.toJson.toString)
      context.stop(self)
    }
    case _=>{
      log.error("invalid")
    }
  }
}