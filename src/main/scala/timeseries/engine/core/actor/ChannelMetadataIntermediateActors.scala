package timeseries.engine.core.actor

import java.util.UUID

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import timeseries.engine.common.data.{ChannelMetadata, Index}
import timeseries.engine.common.msg.{ChannelMetadataAdded, ChannelMetadataFailedToAdd}
import timeseries.engine.core.actor.AddChannelMetadataIntermediateActor.CheckCount
import timeseries.engine.core.actor.IndicesChannelMetadataActor.StartIndex
import timeseries.engine.dao.cassandra.StaticChannelMetadata
import timeseries.engine.store.cassandra.repository.`trait`.PrimitiveDynamicChannelMetadata

object AddChannelMetadataIntermediateActor{
  def props(correlationId:UUID, channelMetadata:ChannelMetadata, replyTo : ActorRef) = Props(new AddChannelMetadataIntermediateActor(correlationId:UUID, channelMetadata, replyTo))
  def name(correlationId:UUID) = s"AddChannelMetadataIntermediateActor-${correlationId.toString}"
  object CheckCount
}

final class AddChannelMetadataIntermediateActor(val correlationId:UUID, channelMetadata:ChannelMetadata, val replyTo : ActorRef) extends Actor with ActorLogging{
  var successfulAdd : List[BaseChannelMetadataAdded] = Nil
  var failedToAddAdd : List[BaseChannelMetadataFailedToAdd] = Nil
  var count = 0

  override def receive = {
    case channelMetadataAdded : BaseChannelMetadataAdded => {
      successfulAdd = channelMetadataAdded :: successfulAdd
      count = count + 1
      self ! CheckCount
    }
    case channelMetadataFailedtoAdd : BaseChannelMetadataFailedToAdd => {
      failedToAddAdd = channelMetadataFailedtoAdd :: failedToAddAdd
      count = count + 1
      self ! CheckCount
    }
    case CheckCount => {
      if(count >= 2){
        if(successfulAdd.length >= 2){
          replyTo ! ChannelMetadataAdded(channelMetadata)
        }else{
          replyTo ! ChannelMetadataFailedToAdd(channelMetadata, failedToAddAdd.map(_.e))
        }
        context.stop(self)
      }
    }
  }
}


object GetChannelMetadataIntermediateActor{
  def props(id: UUID, correlationId:UUID, replyTo : ActorRef) = Props(new GetChannelMetadataIntermediateActor(id, correlationId:UUID, replyTo))
  def name(correlationId:UUID) = s"GetChannelMetadataIntermediateActor-${correlationId.toString}"
  object CheckCount
}

final class GetChannelMetadataIntermediateActor(val id: UUID, val correlationId:UUID, val replyTo : ActorRef) extends Actor with ActorLogging{
  var staticChannelMetadata : Option[StaticChannelMetadata] = None
  var primitiveDynamicChannelMetadata : Option[PrimitiveDynamicChannelMetadata] = None
  var startIndex : Option[StartIndex] = None
  var count = 0
  var hasReplied = false
  final val maxCount = 3

  override def receive = {
    case scm : StaticChannelMetadata => {
      staticChannelMetadata = Some(scm)
      count = count + 1
      self ! CheckCount
    }
    case pdcm : PrimitiveDynamicChannelMetadata => {
      primitiveDynamicChannelMetadata = Some(pdcm)
      count = count + 1
      self ! CheckCount
    }
    case sIndex : StartIndex => {
      startIndex = Some(sIndex)
      count = count + 1
      self ! CheckCount
    }
    case CheckCount => {

      def getEmptyChannelMetadata() : ChannelMetadata = {
        new ChannelMetadata(id,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,false)
      }

      def mapFromStaticChannelMetadata(originalChannelMetadata : ChannelMetadata, staticChannelMetadata: StaticChannelMetadata) : ChannelMetadata= {
        val scm = timeseries.engine.common.convert.ModuleConverters.AsChannelMetadata(staticChannelMetadata)
        originalChannelMetadata.copy(
          `type` = scm.`type`,
          externalType = scm.externalType,
          annotation = scm.annotation,
          uri = scm.uri,
          dataType = scm.dataType,
          dataSetType = scm.dataSetType,
          customDataType = scm.customDataType,
          mnemonic = scm.mnemonic,
          uom = scm.uom,
          additionalIndices = scm.additionalIndices,
          extensions = scm.extensions,
          description = scm.description,
          timelineId = scm.timelineId,
          sources = scm.sources,
          bucketInfo = scm.bucketInfo,
          indexType = scm.indexType,
          indexUOM = scm.indexUOM,
          indexDTM = scm.indexDTM,
          indexDirection = scm.indexDirection,
          indexMnemonic = scm.indexMnemonic,
          indexDescription = scm.indexDescription,
          indexUri = scm.indexUri,
          changeTime = scm.changeTime,
          creationTime = scm.creationTime,
          isDeleted = scm.isDeleted
        )
      }

      def mapFromPrimitiveDynamicChannelMetadata(originalChannelMetadata : ChannelMetadata, primitiveDynamicChannelMetadata: PrimitiveDynamicChannelMetadata) : ChannelMetadata= {
        val tempcmd = timeseries.engine.common.convert.ModuleConverters.AsChannelMetadata(primitiveDynamicChannelMetadata)

        originalChannelMetadata.copy(
          modificationTime = tempcmd.modificationTime,
          status = tempcmd.status
        )
      }

      def mapFromStartIndex(originalChannelMetadata : ChannelMetadata, startIndex: StartIndex) : ChannelMetadata={
        val si: Index = if(startIndex.index.isDefined) startIndex.index.get else null
        originalChannelMetadata.copy(
          startIndex = si
        )
      }

      if(!hasReplied){
        //check if modification time == creation time, ignore startIndex if hasn't been fetched yet
        if(staticChannelMetadata.isDefined && primitiveDynamicChannelMetadata.isDefined && !startIndex.isDefined){
          if(staticChannelMetadata.get.creationTime.isDefined && primitiveDynamicChannelMetadata.get.mTime.isDefined){
            val crTime = staticChannelMetadata.get.creationTime.get
            val mTime = new org.joda.time.DateTime(primitiveDynamicChannelMetadata.get.mTime.get)
            if(crTime == mTime){
              var channelMetadata = getEmptyChannelMetadata()
              staticChannelMetadata.map(x => mapFromStaticChannelMetadata(channelMetadata, x)).map(x => channelMetadata = x)
              primitiveDynamicChannelMetadata.map(x => mapFromPrimitiveDynamicChannelMetadata(channelMetadata, x)).map(x => channelMetadata = x)
              replyTo ! channelMetadata
              hasReplied = true
            }
          }
        }
        else if(count >= maxCount){
          var channelMetadata = getEmptyChannelMetadata()
          staticChannelMetadata.map(x => mapFromStaticChannelMetadata(channelMetadata, x)).map(x => channelMetadata = x)
          primitiveDynamicChannelMetadata.map(x => mapFromPrimitiveDynamicChannelMetadata(channelMetadata, x)).map(x => channelMetadata = x)
          startIndex.map(x => mapFromStartIndex(channelMetadata, x)).map(x => channelMetadata = x)
          replyTo ! channelMetadata
          hasReplied = true // no need
          context.stop(self)
        }
      }
      else{
        //if startIndex has been ignored, when gotten, stop the actor
        if(count >= maxCount){
          context.stop(self)
        }
      }
    }
    case unknown => {
      log.warning(s"unknown message $unknown")
    }
  }
}