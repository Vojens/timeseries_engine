package timeseries.engine.core.actor

import java.util.UUID

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import timeseries.engine.common.data.{Index, Type}
import timeseries.engine.core.actor.IndicesChannelMetadataActor.{AddStartChannelMetadata, GetStartChannelMetadata, Init, StartIndex}
import timeseries.engine.dao.cassandra.StaticChannelMetadata
import timeseries.engine.store.cassandra.repository.`trait`.{DynamicChannelMetadataRepository, RichDynamicChannelMetadataRepository}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success, Try}

object IndicesChannelMetadataActor{
  def props(id:UUID,  dynamicChannelMetadataRepository : DynamicChannelMetadataRepository) = Props(new IndicesChannelMetadataActor(id, dynamicChannelMetadataRepository))
  def name(id:UUID) = s"indices-${id.toString}"
  case class Init(staticChannelMetadata : StaticChannelMetadata)
  case class GetStartChannelMetadata(id:UUID, replyTo : ActorRef)
  case class AddStartChannelMetadata(id:UUID, startIndex : Index, replyTo : ActorRef)
  val NullChannelMetadata = timeseries.engine.common.msg.NullChannelMetadata
  case class StartIndex(id:UUID, index : Option[Index])
}

class IndicesChannelMetadataActor(val id:UUID,  dynamicChannelMetadataRepository : DynamicChannelMetadataRepository) extends Actor with ActorLogging{

  val rdcmt = dynamicChannelMetadataRepository.asInstanceOf[RichDynamicChannelMetadataRepository]

  var staticChannelMetadata : StaticChannelMetadata = _
  override def receive = init

  def init : Receive = {
    case init:Init => {
      if(init.staticChannelMetadata != null){
        staticChannelMetadata = init.staticChannelMetadata
        staticChannelMetadata.`type` match {
          case Some(_) => {
            context.become(active)
          }
          case _  => {
            //TODO reply back or throw some exception
            log.warning("something went wrong 1")
          }
        }
      }else{
        //TODO reply back or throw some exception
        log.warning(s"static channel metadata is null for id $id")
      }
    }
    case unknown => {
      //TODO reply back or throw some exception
      log.warning(s"has not been initialized yet, and got a message $unknown")
    }
  }

  def active : Receive = {
    case getStartIndex: GetStartChannelMetadata => {
      staticChannelMetadata.`type` match {
        case Some(typByte) => {
          val typ = Type(typByte.intValue())
          log.debug(s"trying to get start index for id $id and type $typ")
          rdcmt.getStartIndex(staticChannelMetadata.id, typ)
            .transform{
              case Success(s) => {
                log.debug(s"success fetching startIndex $s for id $id")
                Try(StartIndex(id, s))
              }
              case Failure(f) => {
                f.printStackTrace()
                log.error(s"failed fetching startIndex ${f} for id $id")
                Try(StartIndex(id, None))
              }
            }
            .foreach(x => {
              getStartIndex.replyTo ! x
            })

        }
        case None => {
          log.error(s"null static channel metadata for id = $id")
          getStartIndex.replyTo ! IndicesChannelMetadataActor.NullChannelMetadata
        }
      }
    }
    case addStartIndex:AddStartChannelMetadata => {
      rdcmt.addStartIndex(id, addStartIndex.startIndex)
        .transform{
          case Success(_) => Try(IndexChannelMetadataAdded(id, addStartIndex.startIndex))
          case Failure(f) => Try(IndexChannelMetadataFailedToAdd(id, addStartIndex.startIndex, f))
        }.map{
        addStartIndex.replyTo ! _
      }
    }
  }
}
