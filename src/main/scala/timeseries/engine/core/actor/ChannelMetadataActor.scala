package timeseries.engine.core.actor

import java.util.UUID

import akka.actor.{Actor, ActorLogging, Props}
import com.datastax.driver.core.ResultSet
import timeseries.engine.common.msg._
import timeseries.engine.core.actor.ChannelMetadataActor.PoisonPillIfNoInsert
import timeseries.engine.dao.cassandra.{StaticChannelMetadata, ToBeDeletedChannel}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

object ChannelMetadataActor{
  def props(id:UUID, channelRepositories : ChannelRepositories) = Props(new ChannelMetadataActor(id, channelRepositories))
  def name(id:UUID) = id.toString

  type AddChannelMetadata = timeseries.engine.common.msg.AddChannelMetadata
  type GetChannelMetadata = timeseries.engine.common.msg.GetChannelMetadata
  type MetadataExists = timeseries.engine.common.msg.ChannelMetadataExists
  val ChannelMetadataAdded = timeseries.engine.common.msg.ChannelMetadataAdded
  val ChannelMetadataFailedToAdd = timeseries.engine.common.msg.ChannelMetadataFailedToAdd
  object PoisonPillIfNoInsert
}

class ChannelMetadataActor(val id: UUID, val channelRepositories : ChannelRepositories) extends Actor with ActorLogging{
  import scala.concurrent.ExecutionContext.Implicits.global
  var staticChannelMetadata:StaticChannelMetadata = _

  //create child actors
  val statusFSMActor = context.actorOf(
    StatusFSMActor.props(id, channelRepositories.dynamicChannelMetadataRepository),
    StatusFSMActor.name(id)
  )

  val indicesChannelMetadataActor = context.actorOf(
    IndicesChannelMetadataActor.props(id, channelRepositories.dynamicChannelMetadataRepository),
    IndicesChannelMetadataActor.name(id)
  )

  override def preStart(): Unit = {
    super.preStart()

    val f: Future[Option[StaticChannelMetadata]] = channelRepositories.staticChannelMetadataRepository.getMetadata(id)
    f.map(x => {
      if(x.isDefined){
        staticChannelMetadata = x.get
        indicesChannelMetadataActor ! IndicesChannelMetadataActor.Init(staticChannelMetadata)
      }else{
        context.system.scheduler.scheduleOnce(60 seconds, self, PoisonPillIfNoInsert)
      }
    })

    Await.result(f, 10000 millis)
  }

//  private def initStaticChannelMetadata(scmd : StaticChannelMetadata): Unit ={
//    indicesChannelMetadataActor ! IndicesChannelMetadataActor.Init(scmd)
//
//    class DummyActor extends Actor{
//      override def receive = {case _ => {}}
//    }
//
//    //dynamic channel metadata
//    val pdcm = timeseries.engine.common.convert.ModuleConverters.AsPrimitiveDynamiChannelMetadata(metadata)
//    statusFSMActor ! StatusFSMActor.AddStatusChannelMetadata(pdcm, context.actorOf(Props[DummyActor]))
//  }

  override def receive = {
    case addIfNotExists:ChannelMetadataActor.AddChannelMetadata => {
      val metadata = addIfNotExists.metadata
      val replyTo = addIfNotExists.replyTo
      //TODO uncomment if there is no need to channel update
      if(staticChannelMetadata != null){
        //return already exists
        //reply directly to the replyTo actor
        replyTo ! ChannelMetadataExists(staticChannelMetadata.id)
      }else{
        //intermediate (middle layer) actor
        val correlationId = UUID.randomUUID()
        val addChannelMetadataIntermediateActor =
          context.actorOf(
            AddChannelMetadataIntermediateActor.props(correlationId, metadata, replyTo),
            AddChannelMetadataIntermediateActor.name(correlationId)
          )
        //static channel metadata
        val scm = timeseries.engine.common.convert.ModuleConverters.AsStaticChannelMetadata(metadata)
        val f: Future[ResultSet] = channelRepositories.staticChannelMetadataRepository.addMetadata(scm)
        f.onComplete{
          case Success(_) => {
            addChannelMetadataIntermediateActor ! StaticChannelMetadataAdded(scm)
          }
          case Failure(s) => {
            addChannelMetadataIntermediateActor ! StaticChannelMetadataFailedToAdd(scm, s)
            context.system.scheduler.scheduleOnce(60 seconds, self, PoisonPillIfNoInsert)
          }
        }

        //if we move this up into Success(_), there is no guarantee that GetChannelMetadata,
        //would come before/after onComplete
        indicesChannelMetadataActor ! IndicesChannelMetadataActor.Init(scm)
        //dynamic channel metadata
        val pdcm = timeseries.engine.common.convert.ModuleConverters.AsPrimitiveDynamiChannelMetadata(metadata)
        statusFSMActor ! StatusFSMActor.AddStatusChannelMetadata(pdcm, addChannelMetadataIntermediateActor)


        //TODO do it in a better way where it stores a temp value that it's under process
        //but it's kept in memory meanwhile
        //when it's fully processes, the intermediate actor should act accordingly
        staticChannelMetadata = scm
      }
    }
    case PoisonPillIfNoInsert => {
      //if no channelMetadata added for 1 minute, stop this actor.
      if(staticChannelMetadata == null){
        context.stop(self)
      }
    }
    case getChannelMetadata:ChannelMetadataActor.GetChannelMetadata => {

      if(staticChannelMetadata == null){
        //return already exists
        //reply directly to the replyTo actor
        getChannelMetadata.replyTo ! ChannelMetadataNotExists(getChannelMetadata.id)
      }else{
        val replyTo = getChannelMetadata.replyTo

        if(staticChannelMetadata.isDeleted.isDefined && staticChannelMetadata.isDeleted.get){
          replyTo ! DeletedChannelMetaData(staticChannelMetadata.id)
        }
        else{
          //intermediate (middle layer) actor
          val correlationId = UUID.randomUUID()
          val getChannelMetadataIntermediateActor =
            context.actorOf(
              GetChannelMetadataIntermediateActor.props(getChannelMetadata.id, correlationId, replyTo),
              GetChannelMetadataIntermediateActor.name(correlationId)
            )

          getChannelMetadataIntermediateActor ! staticChannelMetadata
          statusFSMActor ! StatusFSMActor.GetStatusChannelMetadata(getChannelMetadata.id, getChannelMetadataIntermediateActor)
          indicesChannelMetadataActor ! IndicesChannelMetadataActor.GetStartChannelMetadata(getChannelMetadata.id, getChannelMetadataIntermediateActor)
        }
      }
    }
    case deleteChannelMetaData:DeleteChannelMetadata => {
      val deleteMetaDataByDelFlagFuture = channelRepositories.staticChannelMetadataRepository.deleteMetaDataByDelFlag(deleteChannelMetaData.id)
      val addToBeDeletedChannelFuture = channelRepositories.toBeDeletedChannelRepository.addToBeDeletedChannel(ToBeDeletedChannel(deleteChannelMetaData.id))
      val of: Future[(ResultSet, ResultSet)] = for{
        f1Result <- deleteMetaDataByDelFlagFuture
        f2Result <- addToBeDeletedChannelFuture
      }yield (f1Result, f2Result)

      of.onComplete{
        case Success(_) => {
          staticChannelMetadata = staticChannelMetadata.copy(isDeleted = Some(true))
          deleteChannelMetaData.replyTo ! ChannelDeleted(deleteChannelMetaData.id)
          context.system.scheduler.scheduleOnce(60 seconds, self, PoisonPillIfNoInsert)
        }
        case Failure(s) => {
          deleteChannelMetaData.replyTo ! ChannelDeletionFailed(deleteChannelMetaData.id, s)
        }
      }
    }
  }
}
