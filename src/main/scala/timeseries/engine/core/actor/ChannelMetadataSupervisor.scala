package timeseries.engine.core.actor

import akka.actor.{Actor, ActorLogging, Props}
import timeseries.engine.common.msg._
import timeseries.engine.store.cassandra.repository.`trait`._

case class ChannelRepositories(
                                staticChannelMetadataRepository : StaticChannelMetadataRepository,
                                dynamicChannelMetadataRepository : DynamicChannelMetadataRepository,
                                toBeDeletedChannelRepository : ToBeDeletedChannelRepository,
                                channelBucketTimelineRepository: ChannelBucketTimelineRepository,
                                defaultBucketInfoRepository: DefaultBucketInfoRepository,
                                doubleChannelRepository: DoubleChannelRepository,
                                timestampChannelRepository: TimestampChannelRepository
                            )

object ChannelMetadataSupervisor{
  def props(channelRepositories : ChannelRepositories) = Props(new ChannelMetadataSupervisor(channelRepositories))
  def name(name:String) = name
  type AddChannelMetadata = timeseries.engine.common.msg.AddChannelMetadata
  type GetChannelMetadata = timeseries.engine.common.msg.GetChannelMetadata
}

class ChannelMetadataSupervisor(channelRepositories : ChannelRepositories) extends Actor with ActorLogging{
  override def receive = {
    case addChannelMetadata:AddChannelMetadata => {
      //println("Inisde addChannelMetadataMsg")
      val metadataActor = context.child(addChannelMetadata.metadata.id.toString) match {
        case Some(child) => child
        case None => context.actorOf(ChannelMetadataActor.props(addChannelMetadata.metadata.id, channelRepositories), ChannelMetadataActor.name(addChannelMetadata.metadata.id))
      }
      metadataActor ! addChannelMetadata
    }
    case getChannelMetaData:GetChannelMetadata => {
       //println("Inisde getChannelMetaDataMsg")
       val metaDataActor = context.child(getChannelMetaData.id.toString) match {
        case Some(child) => child
        case None => {
          log.info(s"creating a channel metadata actor with id $getChannelMetaData.id")
          context.actorOf(ChannelMetadataActor.props(getChannelMetaData.id, channelRepositories), ChannelMetadataActor.name(getChannelMetaData.id))
        }
      }
       metaDataActor ! getChannelMetaData
    }
    case deleteChannelMetaData:DeleteChannelMetadata => {
       //println("Inisde getChannelMetaDataMsg")
       val metaDataActor = context.child(deleteChannelMetaData.id.toString) match {
        case Some(child) => child
        case None => context.actorOf(ChannelMetadataActor.props(deleteChannelMetaData.id, channelRepositories), ChannelMetadataActor.name(deleteChannelMetaData.id))
      }
       metaDataActor ! deleteChannelMetaData
    }
  }
}