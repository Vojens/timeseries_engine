package timeseries.engine.core.actor

import java.util.UUID

import timeseries.engine.common.data.Index
import timeseries.engine.dao.cassandra.StaticChannelMetadata
import timeseries.engine.store.cassandra.repository.`trait`.PrimitiveDynamicChannelMetadata

private [core] trait BaseChannelMetadataAdded
case class StaticChannelMetadataAdded(staticChannelMetadata: StaticChannelMetadata) extends BaseChannelMetadataAdded
case class StatusChannelMetadataAdded(metadata:PrimitiveDynamicChannelMetadata) extends BaseChannelMetadataAdded
case class IndexChannelMetadataAdded(id: UUID, index:Index) extends BaseChannelMetadataAdded

private [core] sealed abstract class BaseChannelMetadataFailedToAdd{
  def e : Throwable
}
private [core] case class StaticChannelMetadataFailedToAdd(staticChannelMetadata: StaticChannelMetadata, val e : Throwable) extends BaseChannelMetadataFailedToAdd
private [core] case class StatusChannelMetadataFailedToAdd(primitiveDynamicChannelMetadata: PrimitiveDynamicChannelMetadata, val e : Throwable) extends BaseChannelMetadataFailedToAdd
private [core] case class IndexChannelMetadataFailedToAdd(id: UUID, index: Index, val e : Throwable) extends BaseChannelMetadataFailedToAdd


