package timeseries.engine.services

import java.util.UUID

import akka.actor.{ActorRef, ActorSystem}
import com.datastax.driver.core.Session
import timeseries.engine.common.data.ChannelMetadata
import timeseries.engine.common.msg.{AddChannelMetadata, DeleteChannelMetadata, GetChannelMetadata}
import timeseries.engine.core.actor.{ChannelMetadataSupervisor, ChannelRepositories}
import timeseries.engine.main.ActorSystemInitilizer
import timeseries.engine.store.cassandra.repository.impl._
import com.typesafe.config.ConfigFactory
import akka.event.LoggingAdapter
import akka.event.Logging

// this is the interface to call the metadata actors
object MetadataService {
  
  val system:ActorSystem=ActorSystemInitilizer.system
  val config=ConfigFactory.load("cql")
  val session:Session=ActorSystemInitilizer.session
   val logger: LoggingAdapter=Logging(system, getClass)
  val staticChannelMetdaDataRepository=new StaticChannelMetadataRepositoryImpl(config,session)
  val dynamicChannelMetaDataRepository = new DynamicChannelMetadataRepositoryImpl(config, session)
  val toBeDeletedChannelRepository = new ToBeDeletedChannelRepositoryImpl(config, session)
  val channelBucketTimelineRepository = new ChannelBucketTimelineRepositoryImpl(config, session)
  val defaultBucketInfoRepository = new DefaultBucketInfoRepositoryImpl(config, session)
  val doubleChannelRepository = new DoubleChannelRepositoryImpl(config, session)
  val timestampChannelRepository = new TimestampChannelRepositoryImpl(config, session)

  val channelRepositories = new ChannelRepositories(
    staticChannelMetdaDataRepository,
    dynamicChannelMetaDataRepository,
    toBeDeletedChannelRepository,
    channelBucketTimelineRepository,
    defaultBucketInfoRepository,
    doubleChannelRepository,
    timestampChannelRepository
  )
  logger.info("creating metadataActor")
  //val staticMetaDataActor=system.actorOf(ChannelMetadataSupervisor.props(channelRepositories),"metadataActor")
  val metadataSupervisorActor = system.actorOf(ChannelMetadataSupervisor.props(channelRepositories), ChannelMetadataSupervisor.name("ChannelMetadataSupervisor"))
  println("created metadataActor")

  def AddMetadata(metadata: ChannelMetadata, actorRef: ActorRef) : Unit = {
    val addChannelMetaData = AddChannelMetadata(metadata, actorRef)
    metadataSupervisorActor ! addChannelMetaData
  }

  def GetMetadata(id: UUID, actorRef: ActorRef) : Unit = {
    val getChannelMetaData = GetChannelMetadata(id,actorRef)
    metadataSupervisorActor ! getChannelMetaData
  }
  
  def deleteMetadata(id: UUID, actorRef: ActorRef) : Unit = {
    logger.info(id+" "+actorRef)
    val getChannelMetaData = DeleteChannelMetadata(id,actorRef)
    metadataSupervisorActor ! getChannelMetaData
  }
}
