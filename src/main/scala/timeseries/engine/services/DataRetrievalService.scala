package timeseries.engine.services

import java.util.UUID
import timeseries.engine.store.cassandra.query.ChannelQueries
import com.datastax.driver.core.Session
import timeseries.engine.dao.cassandra.StaticChannelMetadata

class DataRetrievalService {
  val session:Session = null
  def fetchChannelMetaData(channelId:UUID) :StaticChannelMetadata={
    val boundedStatement=ChannelQueries.selectChannelMetaData.bind(channelId)
    val resultSet=session.execute(boundedStatement)
    println(resultSet)
    val channelMetaData:StaticChannelMetadata=null
    channelMetaData
  }
}