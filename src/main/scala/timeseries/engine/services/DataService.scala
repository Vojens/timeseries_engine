package timeseries.engine.services

import java.util.{Date, UUID}

import com.datastax.driver.core.{BatchStatement, Session}
import timeseries.engine.common.data.ChannelMetadata
import timeseries.engine.constants.Constants
import timeseries.engine.module.{ChannelDataInsertReq, DeleteChannelDataReq, DeleteChannelReq, ResponseObj}
import timeseries.engine.store.cassandra.query.ChannelQueries

class DataService{
  
  implicit val session : Session=null
  def deleteChannelData(reqObj:DeleteChannelDataReq): ResponseObj={
    val responseObj:ResponseObj=null;
    //TODO fetch timeline info from cache and fetch bucket info for the cache
    //TODO check if the data range covers an entire timeline, if so delete entries 
      //from timeline table also
    //TODO use batch statements to remove bulk amount of channel data
    println(reqObj)
    responseObj
  }
  def deleteChannel(reqObj:DeleteChannelReq): ResponseObj={ 
    var responseObj:ResponseObj=null;
    val channelId=UUID.fromString(reqObj.id)
    val batchStatment:BatchStatement=new BatchStatement()
    val deleteChannelStatment=ChannelQueries.deleteChannelMetaData
    var boundedStatment=deleteChannelStatment.bind(channelId)
    batchStatment.add(boundedStatment)
    boundedStatment=ChannelQueries.deleteChannelMetaDataSetDelFlag.bind(channelId)
    batchStatment.add(boundedStatment)
    boundedStatment=ChannelQueries.insertToBeDeletedChannel.bind(channelId)
    batchStatment.add(boundedStatment)
    boundedStatment=ChannelQueries.insertDeletedChannel.bind(channelId,reqObj.deleteReason,new Date())
    batchStatment.add(boundedStatment)
    //TODO write methods to delete channel data, bucket info, update cache related to bucket and channel
    session.execute(batchStatment)
    responseObj=ResponseObj(Constants.success,Constants.successMsg)
    responseObj
  }
  
  def saveOrUpdateChannelMetaData(channelMetaData:ChannelMetadata):ResponseObj={
    val responseObj:ResponseObj=null;
    println(channelMetaData)  //TODO remove
    //val boundStatement:BoundStatement=bindChannelMetaData(channelMetaData)
    //session.execute(boundStatement)
    //TODO write methods to insert into dynamic metadata, update cache entries if applicable,
      //fetch default buket info from db or cache
    responseObj
  }
  
  def saveOrUpdateChannelData(channelDataObj:ChannelDataInsertReq){
    val channelID=UUID.fromString(channelDataObj.header.id)
    println(channelID)
    //TODO fetch channel meta data from cache to know the timeline id and default bucket 
      //info
    //TODO fetch bucket info corresponding to the timeline
    //TODO check if the data range fits into any of the buckets, if so, insert into db
      //with existing bucket, otherwise create a new bucket
    println(channelDataObj)
  }

  //gives compiler error
//  private def bindChannelMetaData(channelReq:StaticChannelMetadata):BoundStatement={
//    val boundedStatement:BoundStatement=ChannelQueries.insertChannelMetaData.bind(
//        channelReq.id,channelReq.additionalIndices,channelReq.annotation,channelReq.bucketInfo,
//        channelReq.creationTime,channelReq.creationTime_Date,
//        channelReq.creationTime_Time,channelReq.changeTime,
//        channelReq.changeTime_Date,channelReq.changeTime_Time,
//        channelReq.customDataType,channelReq.dataSetType,
//        channelReq.dataType,channelReq.description,channelReq.extensions,
//        channelReq.externalType,channelReq.indexDescription,
//        channelReq.indexDirection,channelReq.indexDTM,
//        channelReq.indexMnemonic,channelReq.indexType,
//        channelReq.indexUOM,channelReq.indexUri,
//        channelReq.isDeleted,channelReq.mnemonic,channelReq.sources,
//        channelReq.timelineId,channelReq.`type`,channelReq.uom,channelReq.uri
//    )
//    boundedStatement
//  }
  
}