package timeseries.engine.constants

object Constants {
  val success:String="Success"
  val failed:String="Failed"
  val successMsg:String="Operation successfull"
  val failMsg:String="Operation failed"
  val channelMetadataAdded:String="Channel Metadata added successfully"
  val channelMetadataDeleted:String="Channel Metadata deleted successfully"
  val channelMetaDataNotExists:String ="Channel Metadata do not exists"
  val channelDelted:String = "You are trying to fetch Metadata of a deleted channel" 
  val channelMetaDataAlreadyExists:String = "Channel Metadata already exists"
  
  val dateFormatToParse:String ="dd-MM-yyyy HH:mm:ss.SSS"
  val dateFormatToParseISO8601:String ="yyyy-MM-dd'T'HH:mm:ss.SSSZZ"
  
}