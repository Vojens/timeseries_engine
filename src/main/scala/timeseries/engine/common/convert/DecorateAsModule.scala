package timeseries.engine.common.convert

import java.util.UUID

import timeseries.engine.common.data._
import timeseries.engine.dao.cassandra.StaticChannelMetadata
import timeseries.engine.store.cassandra.repository.`trait`.PrimitiveDynamicChannelMetadata

import scala.collection.JavaConverters._

trait DecorateAsModule {
  implicit def AsChannelMetadata(staticChannelMetadata: StaticChannelMetadata): ChannelMetadata ={
    var metadata:ChannelMetadata=null
    try{
      metadata = new ChannelMetadata(
      staticChannelMetadata.id,
      Type(staticChannelMetadata.`type`.getOrElse(java.lang.Byte.valueOf(0.byteValue())).toInt),
      ExternalType(staticChannelMetadata.externalType.getOrElse(java.lang.Byte.valueOf(0.byteValue())).toInt),
      staticChannelMetadata.annotation.getOrElse(null),
      staticChannelMetadata.uri.getOrElse(null),
      DataType(staticChannelMetadata.dataType.getOrElse(java.lang.Byte.valueOf(0.byteValue())).toInt),
      DataSetType(staticChannelMetadata.dataSetType.getOrElse(java.lang.Byte.valueOf(0.byteValue())).toInt),
      staticChannelMetadata.customDataType.getOrElse(""),
      staticChannelMetadata.mnemonic.getOrElse(""),
      staticChannelMetadata.uom.getOrElse(""),
      staticChannelMetadata.additionalIndices.getOrElse(new java.util.ArrayList[Integer]()).asScala.map(_.toInt).toStream,
      staticChannelMetadata.extensions.getOrElse(new java.util.HashMap[java.lang.String,java.lang.String]()).asScala.toMap,
      staticChannelMetadata.description.getOrElse(""),
      staticChannelMetadata.timelineId.getOrElse(new UUID(0L, 0L)), //TODO maybe as an object or refval
      staticChannelMetadata.sources.getOrElse(new java.util.ArrayList[UUID]()).asScala.toStream,
      staticChannelMetadata.bucketInfo.getOrElse(new java.util.HashMap[java.lang.String, java.lang.String]()).asScala.toMap,
      IndexType(staticChannelMetadata.indexType.getOrElse(java.lang.Byte.valueOf(0.byteValue())).toInt),
      staticChannelMetadata.indexUOM.getOrElse(""),
      staticChannelMetadata.indexDTM.getOrElse(""),
      IndexDirection(staticChannelMetadata.indexDirection.getOrElse(java.lang.Byte.valueOf(0.byteValue())).toInt),
      staticChannelMetadata.indexMnemonic.getOrElse(""),
      staticChannelMetadata.indexDescription.getOrElse(""),
      staticChannelMetadata.indexUri.getOrElse(""),
      new org.joda.time.DateTime(staticChannelMetadata.changeTime.get.getTime), // TODO utalize both changetime_d and changetime_t
      new org.joda.time.DateTime(staticChannelMetadata.creationTime.get.getTime), // TODO utalize both changetime_d and changetime_t)
      null,
      null,
      null,
      null,
      Boolean.box(staticChannelMetadata.isDeleted.getOrElse(false).asInstanceOf[Boolean])
    )
    }catch{
      case exp:Exception=>{
        exp.printStackTrace()
      }
    }
    metadata
  }

  implicit def AsChannelMetadata(primitiveDynamicChannelMetadata: PrimitiveDynamicChannelMetadata) : ChannelMetadata = {
    val mTime = primitiveDynamicChannelMetadata.mTime match {
      case Some(mTime) => new org.joda.time.DateTime(mTime)
      case None => null
    }
    val status = primitiveDynamicChannelMetadata.status match {
      case Some(status) => Status(status)
      case None => null
    }
    val metadata = new ChannelMetadata(
      primitiveDynamicChannelMetadata.id,
      null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
      mTime,
      status,
      null,
      null,
      false
    ) 
    metadata
  }
}
