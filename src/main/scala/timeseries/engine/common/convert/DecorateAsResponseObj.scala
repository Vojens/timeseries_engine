package timeseries.engine.common.convert

import timeseries.engine.common.data.ChannelMetadata
import timeseries.engine.module.ChannelStaticMetadataRespObj
import timeseries.engine.module.MetaDataIndexInfo

trait DecorateAsResponseObj {
  implicit def asChannelMetaDataResponseObj(channelMetaData:ChannelMetadata):ChannelStaticMetadataRespObj={
    val indexInfo = MetaDataIndexInfo(
      channelMetaData.indexType.id,
      channelMetaData.indexUOM,
      channelMetaData.indexDTM,
      channelMetaData.indexDirection.id,
      channelMetaData.indexMnemonic,
      channelMetaData.indexDescription,
      channelMetaData.indexUri
    )
    val channelMetaDataRespObj=ChannelStaticMetadataRespObj(
        channelMetaData.id.toString,
        channelMetaData.`type`.id,
        channelMetaData.externalType.id,
        channelMetaData.annotation,
        channelMetaData.uri,
        channelMetaData.dataType.id,
        channelMetaData.dataSetType.id,
        channelMetaData.customDataType,
        channelMetaData.mnemonic,
        channelMetaData.uom,
        channelMetaData.additionalIndices.toArray,
        channelMetaData.extensions,
        channelMetaData.description,
        channelMetaData.sources.map(x=>x.toString).toArray,
        channelMetaData.bucketInfo,
        indexInfo,
        "",
        channelMetaData.changeTime.toString(),
        channelMetaData.creationTime.toString(),
        channelMetaData.status.id
    )
    channelMetaDataRespObj   
  }
  
}