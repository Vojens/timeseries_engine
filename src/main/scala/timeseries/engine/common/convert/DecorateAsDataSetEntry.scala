package timeseries.engine.common.convert

import java.util.UUID
import scala.collection.mutable.ArrayBuffer
import timeseries.engine.module.DataSetEntry
import timeseries.engine.module.DataPoint
import timeseries.engine.module.Value
import timeseries.engine.module.DoubleValue
import timeseries.engine.module.BigIntValue
import timeseries.engine.module.TextValue
import timeseries.engine.module.DateValue
import java.time.format.DateTimeFormatter
import timeseries.engine.constants.Constants
import java.time.LocalDate
import timeseries.engine.module.BlobValue
import timeseries.engine.common.data.DoubleIndex
import java.time.LocalDateTime
import timeseries.engine.common.data.TimestampIndex

trait DecorateAsDataSetEntry {
  val dateTimeFormatter=DateTimeFormatter.ofPattern(Constants.dateFormatToParseISO8601)
  implicit def doubleChannelAsDataSetEntry(data:Array[Map[String,String]],id:UUID,dataType:Int):
  Either[Throwable,ArrayBuffer[DataSetEntry]]={
    try{
      val dataSetEntryArray=new ArrayBuffer[DataSetEntry]
      var dataPoint:DataPoint=null;
      data.map(x=>{
        val value=populateValue(dataType, x("value"))
        dataPoint=DataPoint(DoubleIndex(x("index").toDouble),value)
        val dataSetEntry=DataSetEntry(id,dataPoint)
        dataSetEntryArray+=dataSetEntry
      })
      Right(dataSetEntryArray)
    }catch{
      case numberFormatException:NumberFormatException=>{
        Left(numberFormatException)
      }
    }
  }
  
  implicit def timeChannelAsDataSetEntry(data:Array[Map[String,String]],id:UUID,dataType:Int):
  Either[Throwable,ArrayBuffer[DataSetEntry]]={
    try{
      val dataSetEntryArray=new ArrayBuffer[DataSetEntry]
      var dataPoint:DataPoint=null;
      data.map(x=>{
        val value=populateValue(dataType, x("value"))
        dataPoint=DataPoint(TimestampIndex(LocalDateTime.parse(x("index"),dateTimeFormatter)),value)
        val dataSetEntry=DataSetEntry(id,dataPoint)
        dataSetEntryArray+=dataSetEntry
      })
      Right(dataSetEntryArray)
    }catch{
      case exception:Exception=>{
        Left(exception)
      }
    }
  }
  private def populateValue(dataType:Int,valueString:String):Value={
    var value:Value=null;
      dataType match{
        case 1=>{
          value=DoubleValue(valueString.toDouble)
        }
        case 2=>{
          value=BigIntValue(valueString.toLong)
        }
        case 3=>{
          value=TextValue(valueString)
        }
        case 4=>{
          value=DateValue(LocalDate.parse(valueString, dateTimeFormatter))
        }
        case 5=>{
          value=BlobValue(valueString.getBytes)
        }
        case _=>{
          throw new Exception("Invalid Datatype")
        }
      }
      value
  }
}