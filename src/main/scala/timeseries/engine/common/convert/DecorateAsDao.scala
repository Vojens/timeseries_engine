package timeseries.engine.common.convert

import java.util.UUID

import com.datastax.driver.core.LocalDate
import timeseries.engine.common.data._
import timeseries.engine.dao.cassandra.StaticChannelMetadata
import timeseries.engine.store.cassandra.repository.`trait`.PrimitiveDynamicChannelMetadata

import scala.collection.JavaConverters.{seqAsJavaList, _}
import timeseries.engine.module.DataSetEntry
import timeseries.engine.dao.cassandra.DoubleChannel

trait DecorateAsDao {
  implicit def AsStaticChannelMetadata(channelMetadata: ChannelMetadata) : StaticChannelMetadata = {

    def NoneWhenElseSome[T](value : T, exception : T) : Option[T] = {
      value match {
        case `exception` => None
        case x => Some(x)
      }
    }

//    def NoneWhenOp1ElseSome[T](value : T, noneWhen : => Boolean) : Option[T] = {
//      value match {
//        case x if noneWhen  => None
//        case y => Some(y)
//      }
//    }

    def NoneWhenOpElseSome[T](value : T, noneWhen : T => Boolean) : Option[T] = {
      value match {
        case _ if noneWhen(value)  => None
        case y => Some(y)
      }
    }

    def isStringNullOrEmpty(s:String): Boolean ={
      s == null || s == ""
    }

    def isListNullOrEmpty[T](l:java.util.List[T]): Boolean ={
      l == null || l.isEmpty
    }

    def isMapNullOrEmpty[T1,T2](m:java.util.Map[T1,T2]): Boolean ={
      m == null || m.isEmpty
    }

    def isUuidNullOrEmpty(u:UUID): Boolean ={
      u == null || u == new UUID(0L, 0L)
    }


    val metadata = new StaticChannelMetadata(
      channelMetadata.id,
      NoneWhenElseSome[java.lang.Byte](Byte.box(channelMetadata.`type`.id.toByte),Byte.box(0.toByte)),
      NoneWhenElseSome[java.lang.Byte](Byte.box(channelMetadata.externalType.id.toByte),Byte.box(0.toByte)),
      NoneWhenOpElseSome[String](channelMetadata.annotation, isStringNullOrEmpty(_)),
      NoneWhenOpElseSome[String](channelMetadata.uri, isStringNullOrEmpty(_)),
      NoneWhenElseSome[java.lang.Byte](channelMetadata.dataType.id.toByte, 0.toByte),
      NoneWhenElseSome[java.lang.Byte](channelMetadata.dataSetType.id.toByte, 0.toByte),
      NoneWhenOpElseSome[String](channelMetadata.customDataType, isStringNullOrEmpty(_)),
      NoneWhenOpElseSome[String](channelMetadata.mnemonic, isStringNullOrEmpty(_)),
      NoneWhenOpElseSome[String](channelMetadata.uom, isStringNullOrEmpty(_)),
      NoneWhenOpElseSome(seqAsJavaList(channelMetadata.additionalIndices).asInstanceOf[java.util.List[java.lang.Integer]], isListNullOrEmpty[Integer](_)),
      NoneWhenOpElseSome(mapAsJavaMap(channelMetadata.extensions), isMapNullOrEmpty[String,String](_)),
      NoneWhenOpElseSome[String](channelMetadata.description, isStringNullOrEmpty(_)),
      NoneWhenOpElseSome[UUID](channelMetadata.timelineId, isUuidNullOrEmpty(_)),
      NoneWhenOpElseSome(seqAsJavaList(channelMetadata.sources), isListNullOrEmpty[UUID](_)),
      NoneWhenOpElseSome(mapAsJavaMap(channelMetadata.bucketInfo), isMapNullOrEmpty[String,String](_)),
      NoneWhenElseSome[java.lang.Byte](channelMetadata.indexType.id.toByte, 0.toByte),
      NoneWhenOpElseSome[String](channelMetadata.indexUOM, isStringNullOrEmpty(_)),
      NoneWhenOpElseSome[String](channelMetadata.indexDTM, isStringNullOrEmpty(_)),
      NoneWhenElseSome[java.lang.Byte](channelMetadata.indexDirection.id.toByte, 0.toByte),
      NoneWhenOpElseSome[String](channelMetadata.indexMnemonic, isStringNullOrEmpty(_)),
      NoneWhenOpElseSome[String](channelMetadata.indexDescription, isStringNullOrEmpty(_)),
      NoneWhenOpElseSome[String](channelMetadata.indexUri, isStringNullOrEmpty(_)),
      channelMetadata.changeTime match {
        case null => None
        case x => Some(x.toDate)
      },
      channelMetadata.changeTime match {
        case null => None
        case x => Some[LocalDate](com.datastax.driver.core.LocalDate.fromMillisSinceEpoch (x.getMillis))
      },
      channelMetadata.changeTime match {
        case null => None
        case x => Some(x.getMillis)
      },
      channelMetadata.creationTime match {
        case null => None
        case x => Some(x.toDate)
      },
      channelMetadata.creationTime match {
        case null => None
        case x => Some[LocalDate](com.datastax.driver.core.LocalDate.fromMillisSinceEpoch (x.getMillis))
      },
      channelMetadata.creationTime match {
        case null => None
        case x => Some(x.getMillis)
      },
      Some[java.lang.Boolean](Boolean.box(channelMetadata.isDeleted))
    )
    metadata
  }

  implicit def AsPrimitiveDynamiChannelMetadata (channelMetadata : ChannelMetadata) : PrimitiveDynamicChannelMetadata = {
    PrimitiveDynamicChannelMetadata(
      channelMetadata.id,
      Some(channelMetadata.status.id.byteValue()),
      Some(channelMetadata.modificationTime.getMillis)
    )
  }
}
