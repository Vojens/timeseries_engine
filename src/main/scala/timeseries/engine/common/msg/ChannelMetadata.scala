package timeseries.engine.common.msg

import java.util.UUID

import akka.actor.ActorRef
import timeseries.engine.common.data.ChannelMetadata
import timeseries.engine.module.ChannelStaticMetadataReqObj

case class AddChannelMetadata(metadata:ChannelMetadata, replyTo:ActorRef)
case class GetChannelMetadata(id:UUID, replyTo:ActorRef)
case class ChannelMetadataExists(id:UUID)
case class ChannelMetadataNotExists(id:UUID)
case class ChannelMetadataAdded(metadata:ChannelMetadata)
case class ChannelMetadataFailedToAdd(metadata:ChannelMetadata, e:List[Throwable])
case class DeleteChannelMetadata(id:UUID, replyTo:ActorRef)
case class ChannelDeleted(id:UUID)
case class ChannelDeletionFailed(id:UUID, e:Throwable)
case class RequestValidationForChannelMetaData(metadataReq:ChannelStaticMetadataReqObj,replyTo:ActorRef)
case class InvalidChannelMetadata(metadata: ChannelMetadata)
object NullChannelMetadata
case class DeletedChannelMetaData(id:UUID)
case class RequestValidationFailed(id:String,msg:String)


