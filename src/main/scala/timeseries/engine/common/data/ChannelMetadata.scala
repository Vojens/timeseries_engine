package timeseries.engine.common.data

import java.util.UUID

import timeseries.engine.common.data.DataSetType.DataSetType
import timeseries.engine.common.data.DataType.DataType
import timeseries.engine.common.data.ExternalType.ExternalType
import timeseries.engine.common.data.IndexDirection.IndexDirection
import timeseries.engine.common.data.IndexType.IndexType
import timeseries.engine.common.data.Status.Status
import timeseries.engine.common.data.Type.Type

object Type extends Enumeration{
  type Type = Value
  val Undefined, Timestamp, Double, DuadDouble = Value
}

object DataType extends Enumeration{
  type DataType = Value
  val Undefined, Double, Long, Text, Ascii, Blob, INetAddress, Int, SmallInt, TinyInt, Uuid, VarInt, Timestamp = Value
}

object DataSetType extends Enumeration{
  type DataSetType = Value
  val Undefined, Raw, Alias, Synthetic, Derived, Threshold, Event, DownSampled = Value
}

object Status extends Enumeration{
  type Status = Value
  val Undefined, Active, Inactive, Closed = Value
}

object IndexType extends Enumeration{
  type IndexType = Value
  val Undefined, Byte, Datetime, Double, Float, Int , Long, Short, String, String40, String16, Unknown = Value
}

object IndexDirection extends Enumeration{
  type IndexDirection = Value
  val Undefined, Increasing, Decreasing, Unknown = Value
}

object ExternalType extends Enumeration{
  type ExternalType = Value
  //TODO
  val Undefined, Timestamp, Double, DoubleRange = Value 
}



case class ChannelMetadata(
                     id:java.util.UUID,
                     `type`:Type,
                     externalType:ExternalType,
                     annotation:String,
                     uri:String,
                     dataType:DataType,
                     dataSetType:DataSetType,
                     customDataType:String,
                     mnemonic:String,
                     uom:String,
                     additionalIndices:Stream[Int],
                     extensions:Map[String,String],
                     description:String,
                     timelineId:UUID,
                     sources:Stream[UUID],
                     bucketInfo:Map[String,String],
                     indexType:IndexType,
                     indexUOM:String,
                     indexDTM:String,
                     indexDirection:IndexDirection,
                     indexMnemonic:String,
                     indexDescription:String,
                     indexUri:String,
                     changeTime:org.joda.time.DateTime,
                     creationTime:org.joda.time.DateTime,
                     modificationTime:org.joda.time.DateTime,
                     status : Status,
                     startIndex : Index,
                     endIndex : Index,
                     isDeleted:Boolean
                   )



object StatusFSM{
  def ToStatus(statusFSM:StatusFSM): Status = {
    statusFSM match {
      case UndefinedFSM => Status.Undefined
      case ActiveFSM => Status.Active
      case InactiveFSM => Status.Inactive
      case ClosedFSM => Status.Closed
    }
  }
  def FromStatus(status:Status) : StatusFSM= {
    status match {
      case Status.Undefined => UndefinedFSM
      case Status.Active => ActiveFSM
      case Status.Inactive => InactiveFSM
      case Status.Closed => ClosedFSM
    }
  }

  def ToByte(statusFSM:StatusFSM): Byte = {
    ToStatus(statusFSM).id.byteValue()
  }

  def FromByte(status:Byte) : StatusFSM= {
    FromStatus(Status(status))
  }
}

sealed trait StatusFSM
case object UndefinedFSM extends StatusFSM
case object ActiveFSM extends StatusFSM
case object InactiveFSM extends StatusFSM
case object ClosedFSM extends StatusFSM

case class StatusFSMData(mTime : org.joda.time.DateTime)