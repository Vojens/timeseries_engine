package timeseries.engine.common.data

import java.time.LocalDateTime



sealed trait Index{
  //type T  //TODO to be implemented after changing joda time to java time
}
case class TimestampIndex(val index:LocalDateTime) extends Index with Ordered[TimestampIndex]{
  override def compare(that: TimestampIndex) = this.index.compareTo(that.index)
}
case class DoubleIndex(val index:Double) extends Index with Ordered[DoubleIndex] {
  override def compare(that: DoubleIndex) = (this.index) compare(that.index)
}
case class DuadDoubleIndex(val element_1:Double, val element_2:Double) extends Index with Ordered[DuadDoubleIndex]{
  override def compare(that: DuadDoubleIndex) = {
    this.element_1 compare(that.element_1) match {
      case 0 => this.element_2 compare(that.element_2)
      case x:Int => x
    }
  }
}


