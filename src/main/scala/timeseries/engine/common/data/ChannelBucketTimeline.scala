package timeseries.engine.common.data

import java.util.UUID

sealed trait ChannelBucketTimeline{
  type T
  def channelId:UUID
  def timelineId:UUID
  def bucket:T
  def bucketId:UUID
}

case class DoubleChannelBucketTimeline(
                                        val channelId:UUID,
                                        val timelineId:UUID,
                                        val bucket : DoubleIndex,
                                        val bucketId : UUID) extends ChannelBucketTimeline{
  type T = DoubleIndex
}

case class TimestampChannelBucketTimeline(
                                        val channelId:UUID,
                                        val timelineId:UUID,
                                        val bucket : TimestampIndex,
                                        val bucketId : UUID) extends ChannelBucketTimeline{
  type T = TimestampIndex
}