package timeseries.engine.store.cassandra.repository.impl

import com.datastax.driver.core.BoundStatement

trait RichRepository {
  def maybeSet[T](i : Int, e : Option[T], func : (Int, T) => BoundStatement)(implicit boundStatement : BoundStatement) : BoundStatement  ={
    if(e.isDefined){
      func(i,e.get)
    }else{
      boundStatement.unset(i)
    }
    boundStatement
  }

  def maybeSet[T](name : String, e : Option[T], func : (String, T) => BoundStatement)(implicit boundStatement : BoundStatement) : BoundStatement  ={
    if(e.isDefined){
      func(name,e.get)
    }else{
      boundStatement.unset(name)
    }
    boundStatement
  }

  // WARN: JUST DO NOT DO THIS. IT USES REFLECTIONS AND WE'RE HERE TO AVOID THAT AT ALL COSTS
  // I AM LEAVING THIS COMMENTED FOR FUTURE WARNINGS
//  def maybeSet[T](i : Int, e : Option[T])(implicit boundStatement : BoundStatement) : BoundStatement  ={
//    if(e.isDefined){
//      import scala.reflect._
//      val ct = classTag[T]
//      val cls: Class[T] = ct.runtimeClass.asInstanceOf[Class[T]]
//      boundStatement.set(i, e.get, cls)
//    }else{
//      boundStatement.unset(i)
//    }
//    boundStatement
//  }
//
//  def maybeSet[T](name : String, e : Option[T])(implicit boundStatement : BoundStatement) : BoundStatement  ={
//    if(e.isDefined){
//      import scala.reflect._
//      boundStatement.set(name, e.get, classTag[T].runtimeClass)
//    }else{
//      boundStatement.unset(name)
//    }
//    boundStatement
//  }
}
