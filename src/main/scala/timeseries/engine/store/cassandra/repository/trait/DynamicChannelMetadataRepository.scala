package timeseries.engine.store.cassandra.repository.`trait`

import java.nio.ByteBuffer
import java.util
import java.util.UUID

import com.datastax.driver.core.{ColumnDefinitions, ExecutionInfo, ResultSet, Row}
import com.google.common.util.concurrent.ListenableFuture
import timeseries.engine.common.data.Type.Type
import timeseries.engine.common.data._
import timeseries.engine.dao.cassandra.DynamicChannelMetadata

import scala.concurrent.Future
import java.time.ZoneOffset
import java.time.Instant

trait DynamicChannelMetadataRepository {
  def addDynamicMetadata(dynamicMetaData:DynamicChannelMetadata): Future[ResultSet]
  def addDynamicMetadataStream(dynamicMetaDataList: Stream[DynamicChannelMetadata]) : Future[ResultSet]
  def getDynamicMetadata(id:UUID,subscript_i:java.lang.Byte) : Future[List[Option[DynamicChannelMetadata]]]
  def getDynamicMetadata(id:UUID,subscript_i:java.lang.Byte,subscript_j:java.lang.Byte) : Future[Option[DynamicChannelMetadata]]
}

case class PrimitiveDynamicChannelMetadata(id:UUID, status: Option[Byte], mTime:Option[Long])

trait RichDynamicChannelMetadataRepository extends DynamicChannelMetadataRepository{
  def getStatusAndMTime(id:UUID): Future[Option[PrimitiveDynamicChannelMetadata]] ={
    case class StatusAndMTime(status : Option[Byte], MTime : Option[Long])
//    def extractStatusAndMTime(dynamicChannelMetadata:List[Option[DynamicChannelMetadata]]): Option[StatusAndMTime] ={
//      dynamicChannelMetadata match {
//        case Some(metadata) => {
//          val statusOption = metadata.filter(m => m.subscript_j == 0).map(x => {
//            import timeseries.engine.Implicits._
//            val bb = x.element
//            val status = bb match {
//              case Some(bb) => Some(bb.toByte)
//              case None => None
//            }
//            status
//          }).head
//
//          val mTimeOption = metadata.filter(m => m.subscript_j == 1).map(x => {
//            import timeseries.engine.Implicits._
//            val bb = x.element
//            val status = bb match {
//              case Some(bb) => Some(bb.toLong)
//              case None => None
//            }
//            status
//          }).head
//          Some(StatusAndMTime(statusOption, mTimeOption))
//        }
//        case None => None
//      }
//    }

    def extractStatusAndMTime(dynamicChannelMetadata:List[Option[DynamicChannelMetadata]]): Option[StatusAndMTime] ={

      val statusOption = dynamicChannelMetadata.filter(m => m.isDefined && m.get.subscript_j == 0).map(x => {
        import timeseries.engine.Implicits._
        val bb = x.get.element
        val status = bb match {
          case Some(bb) => Some(bb.toByte)
          case None => None
        }
        status
      }).headOption.flatten

      val mTimeOption = dynamicChannelMetadata.filter(m => m.isDefined && m.get.subscript_j == 1).map(x => {
        import timeseries.engine.Implicits._
        val bb = x.get.element
        val status = bb match {
          case Some(bb) => Some(bb.toLong)
          case None => None
        }
        status
      }).headOption.flatten
      if(statusOption.isDefined || mTimeOption.isDefined){
        Some(StatusAndMTime(statusOption, mTimeOption))
      }else{
        None
      }
    }

    import scala.concurrent.ExecutionContext.Implicits.global

    //val list: Future[List[Option[DynamicChannelMetadata]]] = getDynamicMetadata(id, Byte.box(1))

    getDynamicMetadata(id, Byte.box(1))
      .map(extractStatusAndMTime)
      .map(x => x match {
        case Some(statusAndMTime) => Some(PrimitiveDynamicChannelMetadata(id, statusAndMTime.status, statusAndMTime.MTime))
        case None => None
      })
  }

  def addStatusAndMTime(psamt : PrimitiveDynamicChannelMetadata) : Future[ResultSet] = {
    import timeseries.engine.Implicits._

    val array = new Array[DynamicChannelMetadata](2)
    val statusElement = psamt.status match {
      case Some(status) => Some(status.toByteBuffer)
      case None => None
    }
    val mTimeElement = psamt.mTime match {
      case Some(mTime) => Some(mTime.toByteBuffer)
      case None => None
    }

    array(0) = DynamicChannelMetadata(psamt.id, byte2Byte(1), byte2Byte(0), statusElement, None)
    array(1) = DynamicChannelMetadata(psamt.id, byte2Byte(1), byte2Byte(1), mTimeElement, None)

    addDynamicMetadataStream(array.toStream)
  }

  def addStartIndex(id : UUID, startIndex: Index): Future[ResultSet] ={
    import timeseries.engine.Implicits._
    startIndex match {
      case doubleIndex : DoubleIndex => {
        val dynamicChannelMetadata = DynamicChannelMetadata(id, byte2Byte(0), byte2Byte(0), Some(doubleIndex.index.toByteBuffer), None)
        addDynamicMetadata(dynamicChannelMetadata)
      }
      case timestampIndex : TimestampIndex =>{
        val dynamicChannelMetadata = DynamicChannelMetadata(id, byte2Byte(0), byte2Byte(0), Some(timestampIndex.index.toInstant(ZoneOffset.UTC).toEpochMilli().toByteBuffer), None)
        addDynamicMetadata(dynamicChannelMetadata)
      }
      case _ : DuadDoubleIndex => {
        //TODO implement later
        Future.successful(new ResultSet {override def one(): Row = ???

          override def getColumnDefinitions: ColumnDefinitions = ???

          override def wasApplied(): Boolean = ???

          override def isExhausted: Boolean = ???

          override def all(): util.List[Row] = ???

          override def getExecutionInfo: ExecutionInfo = ???

          override def getAvailableWithoutFetching: Int = ???

          override def isFullyFetched: Boolean = ???

          override def iterator(): util.Iterator[Row] = ???

          override def getAllExecutionInfo: util.List[ExecutionInfo] = ???

          override def fetchMoreResults(): ListenableFuture[ResultSet] = ???
        })
      }
    }
  }
  def getStartIndex (id: UUID, `type` : Type) : Future[Option[Index]] = {

    def extractStartIndices(dynamicChannelMetadata:List[Option[DynamicChannelMetadata]]): Option[Index] ={

      def extractSingleIndex(index: Byte, des : ByteBuffer => Index) : Option[Index] = {
        dynamicChannelMetadata.filter(m => m.isDefined && m.get.subscript_j == index).map(x=> {
          val bbo = x.get.element
          bbo match {
            case Some(bb) => Some(des(bb))
            case None => None
          }
        }).headOption.flatten
      }

      import timeseries.engine.Implicits._
      `type` match {
        case Type.Double => {
          def byteBufferToDouble(self: ByteBuffer) = DoubleIndex(self.toDouble)
          extractSingleIndex(0, byteBufferToDouble(_))
        }
        case Type.Timestamp => {
          def byteBufferToTimestamp(self: ByteBuffer) = TimestampIndex(Instant.ofEpochMilli(self.toLong).atZone(ZoneOffset.UTC).toLocalDateTime())
          extractSingleIndex(0, byteBufferToTimestamp(_))
        }
      }
    }

    import scala.concurrent.ExecutionContext.Implicits.global
    val indices: Future[Option[Index]] = getDynamicMetadata(id, Byte.box(0))
      .map(extractStartIndices)
    indices
    }
}
