package timeseries.engine.store.cassandra.repository.`trait`

import com.datastax.driver.core.ResultSet
import timeseries.engine.dao.cassandra.DoubleChannel

import scala.concurrent.Future

trait DoubleChannelRepository {
  def addDataPoint(dataPoint:DoubleChannel) : Future[ResultSet]
}
