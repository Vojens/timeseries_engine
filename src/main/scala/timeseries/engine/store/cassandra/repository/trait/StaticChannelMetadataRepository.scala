package timeseries.engine.store.cassandra.repository.`trait`

import java.util.UUID

import com.datastax.driver.core.ResultSet
import timeseries.engine.dao.cassandra.StaticChannelMetadata

import scala.concurrent.Future

trait StaticChannelMetadataRepository {
  def getMetadata(id:UUID) : Future[Option[StaticChannelMetadata]]
  def addMetadata(metadata:StaticChannelMetadata) : Future[ResultSet]
  def deleteMetaDataByDelFlag(id:UUID) : Future[ResultSet]
}
