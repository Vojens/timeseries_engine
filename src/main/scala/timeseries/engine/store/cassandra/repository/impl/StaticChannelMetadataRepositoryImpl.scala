package timeseries.engine.store.cassandra.repository.impl

import java.util.UUID

import com.datastax.driver.core.{ResultSet, ResultSetFuture, Session}
import timeseries.engine.Implicits._
import timeseries.engine.dao.cassandra.StaticChannelMetadata
import timeseries.engine.store.cassandra.mapping.StaticChannelMetadataMapper
import timeseries.engine.store.cassandra.repository.`trait`.StaticChannelMetadataRepository
import timeseries.engine.store.cassandra.repository.statement.StaticChannelMetadataStatements
import com.typesafe.config.Config

import scala.concurrent.Future

class StaticChannelMetadataRepositoryImpl(private [cassandra] val config:Config, session: Session) extends StaticChannelMetadataStatements with StaticChannelMetadataRepository {

  implicit val underlying = session;
  implicit val ec =  scala.concurrent.ExecutionContext.Implicits.global

  //block for now, but do it async later on
  //session.prepareAsync(selectAllMetadata.cql).asScala //uncomment the import above
  val preparedSelectAllMetadata = prepareAndConfigureStatement(selectAllMetadata)
  val preparedSelectMetadata = prepareAndConfigureStatement(selectMetadata)
  val preparedInsertMetadata = prepareAndConfigureStatement(insertMetadata)
  //val preparedDeleteMetadata = prepareAndConfigureStatement(deleteMetadata)
  val preparedDeleteMetaDataByDelFlag = prepareAndConfigureStatement(deleteMetaDataByDelFlag)

  def getMetadata(id:UUID) : Future[Option[StaticChannelMetadata]] = {
    val query: ResultSetFuture = session.executeAsync(preparedSelectMetadata.bind(id))
    val future: Future[ResultSet] = query.asScala

    future.map(x => StaticChannelMetadataMapper.maybeMap(x.one()))
  }
  
  def deleteMetaDataByDelFlag(id:UUID) : Future[ResultSet] = {
    val query: ResultSetFuture = session.executeAsync(preparedDeleteMetaDataByDelFlag.bind(new java.lang.Boolean(true),id))
    query.asScala
  }
  
  

  def addMetadata(metadata:StaticChannelMetadata) : Future[ResultSet] = {
    //TODO use unset
    val preparedStatement = preparedInsertMetadata.bind(
      metadata.id,
      metadata.`type`.getOrElse(null),
      metadata.externalType.getOrElse(null),
      metadata.annotation.getOrElse(null),
      metadata.uri.getOrElse(null),
      metadata.dataType.getOrElse(null),
      metadata.dataSetType.getOrElse(null),
      metadata.customDataType.getOrElse(null),
      metadata.mnemonic.getOrElse(null),
      metadata.uom.getOrElse(null),
      metadata.additionalIndices.getOrElse(null),
      metadata.extensions.getOrElse(null),
      metadata.description.getOrElse(null),
      metadata.timelineId.getOrElse(null),
      metadata.sources.getOrElse(null),
      metadata.indexType.getOrElse(null),
      metadata.indexUOM.getOrElse(null),
      metadata.indexDTM.getOrElse(null),
      metadata.indexDirection.getOrElse(null),
      metadata.indexMnemonic.getOrElse(null),
      metadata.indexDescription.getOrElse(null),
      metadata.indexUri.getOrElse(null),
      metadata.changeTime.getOrElse(null),
      metadata.changeTime_Date.getOrElse(null),
      metadata.changeTime_Time.getOrElse(null),
      metadata.creationTime.getOrElse(null),
      metadata.creationTime_Date.getOrElse(null),
      metadata.creationTime_Time.getOrElse(null),
      metadata.bucketInfo.getOrElse(null),
      metadata.isDeleted.getOrElse(null)
    )
    session.executeAsync(preparedStatement).asScala
  }


}
