package timeseries.engine.store.cassandra.repository.statement

trait StaticChannelMetadataStatements extends Statements{

  private [cassandra] val traitStatementFQN = classOf[StaticChannelMetadataStatements].getName
  private [cassandra] def selectAllMetadata = getStatementConfig('selectAllMetadata)
  private [cassandra] def selectMetadata = getStatementConfig('selectMetadata)
  private [cassandra] def insertMetadata = getStatementConfig('insertMetadata)
  private [cassandra] def deleteMetadata = getStatementConfig('deleteMetadata)
  private [cassandra] def deleteMetaDataByDelFlag = getStatementConfig('deleteMetaDataByDelFlag)

  println(s"keyspace = ${keyspace}, table = ${table}")
}
