package timeseries.engine.store.cassandra.repository.statement

import com.datastax.driver.core.policies.RetryPolicy
import com.datastax.driver.core.{ConsistencyLevel, PreparedStatement, Session}
import timeseries.engine.store.cassandra.util.ConfigUtils
import com.typesafe.config.Config

trait Statements {

  private [cassandra] implicit def config : Config
  private [this] val cqlConfigPath:String = "timeseries.repository.cql"
  private [cassandra] def traitStatementFQN : String
  private [this] def upToTraitStatementFQN = s"${cqlConfigPath}.${traitStatementFQN}"
  lazy val keyspace : String = config.getString(s"${upToTraitStatementFQN}.keyspace")
  lazy val table : String = config.getString(s"${upToTraitStatementFQN}.table")

  private [this] def getFullConfigPath(name : String)(implicit symbol:Symbol) : String = {
    s"${upToTraitStatementFQN}.${symbol.name}.${name}"
  }

  def getStatementConfig(symbol:Symbol) : StatementConfig = {
    import ConfigUtils._
    implicit val sym = symbol
    val cql = config.getString(getFullConfigPath("cql"))
    val tracing = maybeGetValue(getFullConfigPath("enable-tracing"), config.getBoolean(_))
    val consistencyLevel = maybeGetValue[String,ConsistencyLevel](getFullConfigPath("consistency-level"), config.getString(_), ConsistencyLevel.valueOf(_))
    val idempotent = maybeGetValue(getFullConfigPath("idempotent"), config.getBoolean(_))
    val readTimeoutMillis = maybeGetValue(getFullConfigPath("read-timeout-in-millis"), config.getInt(_))
    val retryPolicy = maybeGet[Option[RetryPolicy]](getFullConfigPath("retry-policy"), getRetryPolicy(_)).getOrElse(None)
    val serialConsistencyLevel = maybeGetValue[String,ConsistencyLevel](getFullConfigPath("serial-consistency-level"), config.getString(_), ConsistencyLevel.valueOf(_))
    val fetchSize = maybeGetValue(getFullConfigPath("read-timeout-in-millis"), config.getInt(_))
    new StatementConfig(cql,tracing,consistencyLevel,idempotent,readTimeoutMillis,retryPolicy,serialConsistencyLevel, fetchSize)
  }

  def prepareAndConfigureStatement(statementConfig:StatementConfig)(implicit session : Session): PreparedStatement ={
    try{
      val preparedStatement = session.prepare(statementConfig.cql)
      statementConfig.consistencyLevel.map(preparedStatement.setConsistencyLevel(_))
      statementConfig.idempotent.map(preparedStatement.setIdempotent(_))
      //statementConfig.readTimeoutMillis.map(preparedStatement.)
      statementConfig.retryPolicy.map(preparedStatement.setRetryPolicy(_))
      statementConfig.serialConsistencyLevel.map(preparedStatement.setSerialConsistencyLevel(_))
      def enableTracing(isEnabled:Boolean) = {
        if(isEnabled)
          preparedStatement.enableTracing()
      }
      statementConfig.tracing.map(enableTracing(_))

      preparedStatement
    }catch {
      case x:Throwable => {
        println(x)
        null
      }
    }

  }
}
