package timeseries.engine.store.cassandra.repository.`trait`

import com.datastax.driver.core.ResultSet
import timeseries.engine.dao.cassandra.ToBeDeletedChannel

import scala.concurrent.Future

trait ToBeDeletedChannelRepository {
  def addToBeDeletedChannel(toBeDeletedChannel : ToBeDeletedChannel) : Future[ResultSet]
}
