package timeseries.engine.store.cassandra.repository.impl

import com.datastax.driver.core.Session
import timeseries.engine.dao.cassandra.ToBeDeletedChannel
import timeseries.engine.store.cassandra.repository.`trait`.ToBeDeletedChannelRepository
import timeseries.engine.store.cassandra.repository.statement.ToBeDeletedChannelStatements
import com.typesafe.config.Config
import timeseries.engine.Implicits._

class ToBeDeletedChannelRepositoryImpl(private [cassandra] val config:Config, session: Session) extends ToBeDeletedChannelStatements with ToBeDeletedChannelRepository {

  implicit val underlying = session;
  implicit val ec =  scala.concurrent.ExecutionContext.Implicits.global

  val preparedInsertToBeDeletedChannel = prepareAndConfigureStatement(insertToBeDeletedChannel)

  override def addToBeDeletedChannel(toBeDeletedChannel: ToBeDeletedChannel) = {
    val preparedStatement = preparedInsertToBeDeletedChannel.bind(toBeDeletedChannel.id)
    session.executeAsync(preparedStatement).asScala
  }
}
