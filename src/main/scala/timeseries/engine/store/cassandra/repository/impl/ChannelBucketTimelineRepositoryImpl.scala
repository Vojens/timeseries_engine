package timeseries.engine.store.cassandra.repository.impl

import com.datastax.driver.core.Session
import timeseries.engine.Implicits._
import timeseries.engine.dao.cassandra.ChannelBucketTimeline
import timeseries.engine.store.cassandra.mapping.{ChannelBucketTimelineMapper, Cql3DataTypes}
import timeseries.engine.store.cassandra.repository.`trait`.ChannelBucketTimelineRepository
import timeseries.engine.store.cassandra.repository.statement.ChannelBucketTimelineStatements
import timeseries.engine.store.cassandra.util.PrefetchingResultSetIterator
import com.typesafe.config.Config

class ChannelBucketTimelineRepositoryImpl (private [cassandra] val config:Config, session: Session) extends ChannelBucketTimelineStatements with ChannelBucketTimelineRepository with RichRepository with Cql3DataTypes{
  implicit val underlying = session;
  implicit val ec =  scala.concurrent.ExecutionContext.Implicits.global

  //block for now, but do it async later on
  val preparedInsertChannelBucket = prepareAndConfigureStatement(insertChannelBucket)
  val preparedReadChannelBuckets = prepareAndConfigureStatement(readChannelBuckets)

  override def addChannelTimeline(channelBucketTimeLine: ChannelBucketTimeline) = {
    implicit val bs = preparedInsertChannelBucket.bind()

    bs.set[uuid](0:Int, channelBucketTimeLine.channelId, classOf[uuid])
    bs.set[uuid](1:Int, channelBucketTimeLine.timelineId, classOf[uuid])
    bs.set[blob](2:Int, channelBucketTimeLine.bucket, classOf[blob])
    maybeSet[uuid](3:Int, channelBucketTimeLine.bucketId, bs.setUUID(_:Int,_:uuid))

    underlying.executeAsync(bs).asScala
  }

  override def getChannelTimeline(channelId: uuid, timelineId: uuid) = {

    val bs = preparedReadChannelBuckets.bind(channelId, timelineId)
    if(readChannelBuckets.fetchSize.isDefined) bs.setFetchSize(readChannelBuckets.fetchSize.get)

    underlying.executeAsync(bs).asScala
      .map(
        new PrefetchingResultSetIterator(_, bs.getFetchSize)
          .map(implicit x => ChannelBucketTimelineMapper.maybeMap)
            .flatMap(x => x)
      )
  }
}
