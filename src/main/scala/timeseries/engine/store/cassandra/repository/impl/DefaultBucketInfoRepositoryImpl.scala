package timeseries.engine.store.cassandra.repository.impl

import com.datastax.driver.core.Session
import timeseries.engine.Implicits._
import timeseries.engine.dao.cassandra.DefaultBucketInfo
import timeseries.engine.store.cassandra.mapping.{Cql3DataTypes, DefaultBucketInfoMapper}
import timeseries.engine.store.cassandra.repository.`trait`.DefaultBucketInfoRepository
import timeseries.engine.store.cassandra.repository.statement.DefaultBucketInfoStatements
import com.typesafe.config.Config

class DefaultBucketInfoRepositoryImpl (private [cassandra] val config:Config, session: Session) extends DefaultBucketInfoStatements with DefaultBucketInfoRepository with RichRepository with Cql3DataTypes {
  implicit val underlying = session;
  implicit val ec =  scala.concurrent.ExecutionContext.Implicits.global

  //block for now, but do it async later on
  val preparedInsertDefaultBucketInfo = prepareAndConfigureStatement(insertDefaultBucketInfo)
  val preparedSelectDefaultBucketInfo = prepareAndConfigureStatement(selectDefaultBucketInfo)


  override def addDefaultBucketInfo(defaultBucketInfo: DefaultBucketInfo) = {
    implicit val bs = preparedInsertDefaultBucketInfo.bind()
    bs.set[tinyint](DefaultBucketInfoMapper.enumeration.typ.toString, defaultBucketInfo.`type`, classOf[tinyint])
    bs.set[tinyint](DefaultBucketInfoMapper.enumeration.dattyp.toString, defaultBucketInfo.dataType, classOf[tinyint])
    maybeSet[map[ascii,ascii]](DefaultBucketInfoMapper.enumeration.bktinfo.toString, defaultBucketInfo.bucketInfo, bs.setMap(_:String,_:map[ascii,ascii]))

    underlying.executeAsync(bs).asScala
  }

  override def getDefaultBucketInfo(`type`: tinyint, dataType: tinyint) = {
    val bs = preparedSelectDefaultBucketInfo.bind(`type`, dataType)
    underlying.executeAsync(bs).asScala.map(x => DefaultBucketInfoMapper.maybeMap(x.one()))
  }
}
