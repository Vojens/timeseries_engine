package timeseries.engine.store.cassandra.repository.statement

trait DoubleChannelStatements extends Statements{
  private [cassandra] val traitStatementFQN = classOf[DoubleChannelStatements].getName
  private [cassandra] def insertDataPoint = getStatementConfig('insertDataPoint)
}
