package timeseries.engine.store.cassandra.repository.statement

trait DefaultBucketInfoStatements extends Statements {
  private [cassandra] val traitStatementFQN = classOf[DefaultBucketInfoStatements].getName
  private [cassandra] def insertDefaultBucketInfo = getStatementConfig('insertDefaultBucketInfo)
  private [cassandra] def selectDefaultBucketInfo = getStatementConfig('selectDefaultBucketInfo)
}
