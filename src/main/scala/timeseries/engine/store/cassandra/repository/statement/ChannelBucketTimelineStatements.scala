package timeseries.engine.store.cassandra.repository.statement

trait ChannelBucketTimelineStatements extends Statements{
  private [cassandra] val traitStatementFQN = classOf[ChannelBucketTimelineStatements].getName
  private [cassandra] def insertChannelBucket = getStatementConfig('insertChannelBucket)
  private [cassandra] def readChannelBuckets = getStatementConfig('readChannelBuckets)
}
