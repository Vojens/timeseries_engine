package timeseries.engine.store.cassandra.repository.`trait`

import com.datastax.driver.core.ResultSet
import timeseries.engine.dao.cassandra.ChannelBucketTimeline
import scala.concurrent.Future

trait ChannelBucketTimelineRepository {
  def addChannelTimeline(channelBucketTimeLine : ChannelBucketTimeline) : Future[ResultSet]
  def getChannelTimeline(channelId:java.util.UUID, timelineId:java.util.UUID) : Future[Iterator[ChannelBucketTimeline]]
}
