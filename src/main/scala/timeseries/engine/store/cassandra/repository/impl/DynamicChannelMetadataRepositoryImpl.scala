package timeseries.engine.store.cassandra.repository.impl

import java.util.UUID

import com.datastax.driver.core._
import timeseries.engine.Implicits._
import timeseries.engine.dao.cassandra.DynamicChannelMetadata
import timeseries.engine.exception.MultiplePartitionException
import timeseries.engine.store.cassandra.mapping.DynamicChannelMetadataMapper
import timeseries.engine.store.cassandra.repository.`trait`.{DynamicChannelMetadataRepository, RichDynamicChannelMetadataRepository}
import timeseries.engine.store.cassandra.repository.statement.DynamicChannelMetadataStatements
import com.typesafe.config.Config

import scala.concurrent.Future

class DynamicChannelMetadataRepositoryImpl(private [cassandra] val config: Config, session:Session) extends DynamicChannelMetadataStatements with DynamicChannelMetadataRepository with RichDynamicChannelMetadataRepository{
  private [cassandra] implicit val underlyingSession=session
  private [cassandra] implicit val ec = scala.concurrent.ExecutionContext.Implicits.global

  private [cassandra] val selectAllDynamicDataStatement = prepareAndConfigureStatement(selectAllDynamicMetadata)
  private [cassandra] val selectDynamicDataStatement = prepareAndConfigureStatement(selectDynamicMetadata)
  private [cassandra] val selectDynamicDataListStatement = prepareAndConfigureStatement(selectDynamicMetadataList)
  private [cassandra] val insertDynamicDataStatement = prepareAndConfigureStatement(insertDynamicMetadata)

  private def getAddDynamicMetadataBoundStatement(dynamicMetaData:DynamicChannelMetadata) : BoundStatement = {
    //TODO use unset
    val boundStatement=insertDynamicDataStatement.bind(
      dynamicMetaData.id,
      dynamicMetaData.subscript_i,
      dynamicMetaData.subscript_j,
      dynamicMetaData.element.getOrElse(null),
      dynamicMetaData.tags.getOrElse(null)
    )
    boundStatement
  }

  def addDynamicMetadata(dynamicMetaData:DynamicChannelMetadata): Future[ResultSet] = {
    session.executeAsync(getAddDynamicMetadataBoundStatement(dynamicMetaData)).asScala
  }
  
  def getDynamicMetadata(id:UUID,subscript_i:java.lang.Byte,subscript_j:java.lang.Byte) : Future[Option[DynamicChannelMetadata]] = {
    val query: ResultSetFuture = session.executeAsync(selectDynamicDataStatement.bind(id,subscript_i,subscript_j))
    val future: Future[ResultSet] = query.asScala
    future.map(x => DynamicChannelMetadataMapper.maybeMap(x.one()))
  }

  override def addDynamicMetadataStream(dynamicMetaDataList: Stream[DynamicChannelMetadata]) : Future[ResultSet] = {
    implicit def seq2Distinct[T, C[T] <: Seq[T]](tees: C[T]) = new {
      import collection.generic.CanBuildFrom
      import collection.mutable.{HashSet => MutableHashSet}

      def distinctBy[S](hash: T => S)(implicit cbf: CanBuildFrom[C[T],T,C[T]]): C[T] = {
        val builder = cbf()
        val seen = MutableHashSet[S]()

        for (t <- tees) {
          if (!seen(hash(t))) {
            builder += t
            seen += hash(t)
          }
        }

        builder.result
      }
    }

    if(dynamicMetaDataList.distinctBy(x => (x.id,x.subscript_i)).length > 1){
      throw new MultiplePartitionException()
    }
    val bs = new BatchStatement()
    dynamicMetaDataList.map(getAddDynamicMetadataBoundStatement).foreach(bs.add)
    session.executeAsync(bs).asScala
  }

  override def getDynamicMetadata(id:UUID,subscript_i:java.lang.Byte) : Future[List[Option[DynamicChannelMetadata]]] = {
    import scala.collection.JavaConverters._
    val query: ResultSetFuture = session.executeAsync(selectDynamicDataListStatement.bind(id,subscript_i))
    val future: Future[ResultSet] = query.asScala
    future.map(_.iterator())
        .map(asScalaIterator)
        .map(_.toList.map(implicit row => DynamicChannelMetadataMapper.maybeMap))
  }
}