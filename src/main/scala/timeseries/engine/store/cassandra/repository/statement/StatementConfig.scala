package timeseries.engine.store.cassandra.repository.statement

import com.datastax.driver.core.ConsistencyLevel
import com.datastax.driver.core.policies.RetryPolicy

case class StatementConfig(
                            cql:String,
                            tracing: Option[Boolean],
                            consistencyLevel:Option[ConsistencyLevel],
                            idempotent:Option[Boolean],
                            readTimeoutMillis:Option[Int],
                            retryPolicy:Option[RetryPolicy],
                            serialConsistencyLevel:Option[ConsistencyLevel],
                            fetchSize:Option[Int]
                          )
