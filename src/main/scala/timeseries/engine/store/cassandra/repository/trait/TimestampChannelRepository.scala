package timeseries.engine.store.cassandra.repository.`trait`

import timeseries.engine.dao.cassandra.TimestampChannel

trait TimestampChannelRepository {
  def addDataPoint(dataPoint:TimestampChannel)
}
