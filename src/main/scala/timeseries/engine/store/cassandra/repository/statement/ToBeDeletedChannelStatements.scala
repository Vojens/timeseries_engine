package timeseries.engine.store.cassandra.repository.statement

trait ToBeDeletedChannelStatements extends Statements{
  private [cassandra] val traitStatementFQN = classOf[ToBeDeletedChannelStatements].getName
  private [cassandra] def insertToBeDeletedChannel = getStatementConfig('insertToBeDeletedChannel)
}
