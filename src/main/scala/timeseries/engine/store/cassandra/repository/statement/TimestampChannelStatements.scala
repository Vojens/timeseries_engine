package timeseries.engine.store.cassandra.repository.statement

trait TimestampChannelStatements extends Statements{
  private [cassandra] val traitStatementFQN = classOf[TimestampChannelStatements].getName
  private [cassandra] def insertDataPoint = getStatementConfig('insertDataPoint)
}
