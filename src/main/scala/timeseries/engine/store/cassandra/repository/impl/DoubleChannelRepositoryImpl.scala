package timeseries.engine.store.cassandra.repository.impl

import com.datastax.driver.core.Session
import timeseries.engine.Implicits._
import timeseries.engine.dao.cassandra.DoubleChannel
import timeseries.engine.store.cassandra.mapping.Cql3DataTypes
import timeseries.engine.store.cassandra.repository.`trait`.DoubleChannelRepository
import timeseries.engine.store.cassandra.repository.statement.DoubleChannelStatements
import com.typesafe.config.Config

class DoubleChannelRepositoryImpl(private [cassandra] val config:Config, session: Session) extends DoubleChannelStatements with DoubleChannelRepository with RichRepository with Cql3DataTypes{

  implicit val underlying = session;
  implicit val ec =  scala.concurrent.ExecutionContext.Implicits.global

  //block for now, but do it async later on
  val preparedInsertDataPoint = prepareAndConfigureStatement(insertDataPoint)

  override def addDataPoint(dataPoint: DoubleChannel) = {
    implicit val bs = preparedInsertDataPoint.bind()

    bs.setUUID(0, dataPoint.channelId)
    bs.setUUID(1, dataPoint.timelineId)
    bs.setUUID(2, dataPoint.bucketId)
    bs.setDouble(3, dataPoint.index)

    maybeSet[ascii](4:Int, dataPoint.asciiValue, bs.setString(_:Int,_:ascii))  //asciiValue:       Option[java.lang.String],     //ascii ASCII
    maybeSet[bigint](5:Int, dataPoint.longValue, bs.setLong(_:Int,_:bigint))     //longValue:        Option[java.lang.Long],       //bi 	  bigint
    //maybe use setBytes instead
    maybeSet[blob](6:Int, dataPoint.blobValue, bs.setBytesUnsafe(_:Int,_:blob))        //blobValue:        Option[java.nio.ByteBuffer],  //blb 	BLOB
    maybeSet[double](7:Int, dataPoint.doubleValue, bs.setDouble(_:Int,_:double))   //doubleValue:      Option[java.lang.Double],     //d	    double
    maybeSet[decimal](8:Int, dataPoint.decimalValue, bs.setDecimal(_:Int,_:decimal)) //decimalValue:     Option[java.math.BigDecimal], //dec	  DECIMAL
    maybeSet[date](9:Int, dataPoint.dateValue, bs.setDate(_:Int,_:date))       //dateValue:        Option[java.util.Date],       //dt	  DATE
    maybeSet[int](10:Int, dataPoint.intValue, bs.setInt(_:Int,_:int))       //intValue:         Option[java.lang.Integer],    //i	    int
    maybeSet[inet](11:Int, dataPoint.iNetAddressValue, bs.setInet(_:Int,_:inet)) //iNetAddressValue: Option[java.net.InetAddress], //inet  inet
    maybeSet[smallint](12:Int, dataPoint.smallIntValue, bs.setShort(_:Int,_:smallint))   //smallIntValue:    Option[java.lang.Short],      //si	  smallint
    maybeSet[tinyint](13:Int, dataPoint.tinyIntValue, bs.setByte(_:Int,_:tinyint))    //tinyIntValue:     Option[java.lang.Byte],       //ti	  TINYINT
    maybeSet[time](14:Int, dataPoint.timeValue, bs.setLong(_:Int,_:time))        //timeValue:        Option[java.lang.Long],       //tim	  TIME
    maybeSet[timestamp](15:Int, dataPoint.timestampValue, bs.setTimestamp(_:Int,_:timestamp))  //timestampValue:   Option[java.util.Date],       //ts	  TIMESTAMP
    maybeSet[text](16:Int, dataPoint.textValue, bs.setString(_:Int,_:text))    //textValue:        Option[java.lang.String],     //txt 	text
    maybeSet[uuid](17:Int, dataPoint.uuidValue, bs.setUUID(_:Int,_:uuid))      //uuidValue:        Option[java.util.UUID],       //vi	  varint
    maybeSet[varint](18:Int, dataPoint.varIntValue, bs.setVarint(_:Int,_:varint))  //varIntValue:      Option[java.math.BigInteger], //uid	  UUID


    underlying.executeAsync(bs).asScala
  }
}
