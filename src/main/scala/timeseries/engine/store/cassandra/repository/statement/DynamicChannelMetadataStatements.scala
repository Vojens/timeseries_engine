package timeseries.engine.store.cassandra.repository.statement

trait DynamicChannelMetadataStatements extends Statements{
  private [cassandra] val traitStatementFQN = classOf[DynamicChannelMetadataStatements].getName
  private [cassandra] def selectDynamicMetadata=getStatementConfig('selectDynamicMetadata)
  private [cassandra] def selectDynamicMetadataList=getStatementConfig('selectDynamicMetadataList)
  private [cassandra] def selectAllDynamicMetadata=getStatementConfig('selectAllDynamicMetadata)
  private [cassandra] def insertDynamicMetadata=getStatementConfig('insertDynamicMetadata)
  println(s"keyspace = ${keyspace}, table = ${table}")
}