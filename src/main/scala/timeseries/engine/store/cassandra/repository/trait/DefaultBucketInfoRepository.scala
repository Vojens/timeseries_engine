package timeseries.engine.store.cassandra.repository.`trait`

import com.datastax.driver.core.ResultSet
import timeseries.engine.dao.cassandra.DefaultBucketInfo

import scala.concurrent.Future

trait DefaultBucketInfoRepository {
  def addDefaultBucketInfo(defaultBucketInfo:DefaultBucketInfo) : Future[ResultSet]
  def getDefaultBucketInfo(`type` : java.lang.Byte, dataType : java.lang.Byte) : Future[Option[DefaultBucketInfo]]
}
