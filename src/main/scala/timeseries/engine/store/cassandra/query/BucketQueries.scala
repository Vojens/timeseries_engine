package timeseries.engine.store.cassandra.query

import com.datastax.driver.core.Session

/**
 * Queries to manipulate bucket tables
 */
object BucketQueries {
  
  val session:Session=null;
  
  val insertDefaultBucketInfo=session.prepare("insert into default_bucket_info (typ,dattyp,bktinfo)"+
      " values(?,?,?") 
  val insertDedaultBucketInfoByIndexMnemonic=session.prepare("insert into default_bucket_info_by_idxmnem "+
      " (idxmnem,typ,dattyp,bktinfo) values(?,?,?,?")
}