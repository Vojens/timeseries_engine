package timeseries.engine.store.cassandra.query

import com.datastax.driver.core.Session

/*
 * Queries to manipulate channel meta data 
 */
object ChannelQueries{
  implicit val session : Session=null
  val deleteChannelMetaData=session.prepare("delete from static_channel_metadata where id=?")
  val deleteChannelDynamicMetaData=session.prepare("delete from dynamic_channel_metada where id=? and subscript_i=?")
  val deleteChannelMetaDataSetDelFlag=session.prepare("update static_channel_metadata set isdel=true where id=?")
  
  
  
  val insertDeletedChannel=session.prepare("insert into deleted_channel(id,delres,dtime) values(?,?,?)")
  val insertToBeDeletedChannel=session.prepare("insert into to_be_deleted_channel(id) values(?)")
  val insertChannelMetaData=session.prepare("insert into static_channel_metadata (id,addlidx,annt,bktinfo,crtime,"+
  "crtime_d,crtime_t,ctime,ctime_d,ctime_t,custdattyp,datsettyp,dattyp,descr,extn,exttyp,"+
  "idxdescr,idxdircn,idxdtm,idxmnem,idxtyp,idxuom,idxuri,isdel,mnem,src,timlinid,typ,uom,uri) "+
  "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
  val insertChannelDynamicMetaData=session.prepare("insert into dynamic_channel_metadata (id,subscript_i,"+
  "subscript_j,element,tags) values(?,?,?,?,?)")
  
  val selectChannelMetaData=session.prepare("select * from static_channel_metadata where id=?")
  
  
}