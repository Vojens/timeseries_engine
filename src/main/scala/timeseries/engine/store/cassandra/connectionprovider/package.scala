package timeseries.engine.store.cassandra

import com.datastax.driver.core.{Cluster, Session}

import scala.concurrent.Future

package object connectionprovider {

  implicit class RichCluster(cluster: Cluster){
    def asyncConnect(): Future[Session] ={
      import scala.concurrent.ExecutionContext.Implicits.global
      Future{cluster.connect()}
    }
  }
}
