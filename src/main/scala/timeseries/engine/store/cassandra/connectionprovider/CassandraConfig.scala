package timeseries.engine.store.cassandra.connectionprovider
import java.net.InetSocketAddress
import java.util.concurrent.TimeUnit

import com.datastax.driver.core.policies._
import com.datastax.driver.core.{PercentileTracker, _}
import timeseries.engine.store.cassandra.util.ConfigUtils._
import com.typesafe.config.Config

import scala.collection.JavaConverters._
class CassandraConfig (conf:Config){
implicit val config = conf
  val clusterBuilder = Cluster.builder()
  val defaultPort = 9042
//  val connectionRetries: Int = config.getInt("connect-retries")
//  val connectionRetryDelay: FiniteDuration = config.getDuration("connect-retry-delay", TimeUnit.MILLISECONDS).millis
//
//  val readConsistency: ConsistencyLevel = ConsistencyLevel.valueOf(config.getString("read-consistency"))
//  val writeConsistency: ConsistencyLevel = ConsistencyLevel.valueOf(config.getString("write-consistency"))
//  val forcedReadConsistency: ConsistencyLevel = ConsistencyLevel.valueOf(config.getString("forced-read-consistency"))
//  val forcedRriteConsistency: ConsistencyLevel = ConsistencyLevel.valueOf(config.getString("forced-write-consistency"))
  val port = maybeGetValue[Int]("port", config.getInt(_))//config.getInt("port")
  val contactPoints: Seq[InetSocketAddress] = getContactPoints(config.getStringList("contact-points").asScala, port.getOrElse(defaultPort))
//  val pageSize: Int = config.getInt("page-size")
//  val forcedPageSize: Int = config.getInt("forced-page-size")
//  val clusterName: String = config.getString("cluster-name")
//  val compression:Compression = Compression.valueOf(config.getString("compression"))



  val poolingOptions = maybeGet[PoolingOptions]("pooling-options", getPollingOptions(_))//getPollingOptions(config.getConfig("pooling-options"))
  val queryOptions = maybeGet[QueryOptions]("query-options", getQueryOptions(_))//getQueryOptions(config.getConfig("query-options"))
  val authProvider = (maybeGet[Option[AuthProvider]]("auth-provider", getAuthProvider(_))).getOrElse(None)
  val loadBalancingPolicy = maybeGet[Option[LoadBalancingPolicy]]("load-balancing-policy", getLoadBalancingPolicy(_)).getOrElse(None)
  val socketOptions = maybeGet[SocketOptions]("socket-options", getSocketOptions(_))
  val retryPolicy = maybeGet[Option[RetryPolicy]]("retry-policy", getRetryPolicy(_)).getOrElse(None)
  val speculativeExecutionPolicy = maybeGet[Option[SpeculativeExecutionPolicy]]("speculative-execution-policy", getSpeculativeExecutionPolicy(_)).getOrElse(None)

  private [cassandra] def getContactPoints(contactPoints: Seq[String], port: Int): Seq[InetSocketAddress] = {
    contactPoints match {
      case null | Nil => throw new IllegalArgumentException("A contact point list cannot be empty.")
      case hosts => hosts map {
        ipWithPort =>
          ipWithPort.split(":") match {
            case Array(host, p) => new InetSocketAddress(host, p.toInt)
            case Array(host) => new InetSocketAddress(host, port)
            case msg => throw new IllegalArgumentException(s"A contact point should have the form [host:port] or [host] but was: $msg.")
          }
      }
    }
  }

  private [cassandra] def getPollingOptions(config:Config): PoolingOptions = {
    implicit val conf: Config = config

    val po = new PoolingOptions()

    setIfExists("heart-beat-interval-in-seconds", config.getInt(_), po.setHeartbeatIntervalSeconds(_))
    setIfExists("idle-timeout-in-seconds", config.getInt(_), po.setIdleTimeoutSeconds(_))
    setIfExists("max-queue-size", config.getInt(_), po.setMaxQueueSize(_))
    setIfExists("pool-timeout-in-millis", config.getInt(_), po.setPoolTimeoutMillis(_))

    def getHostDistanceMap(configPath:String):Map[HostDistance,Int] = {
      import scala.collection.JavaConverters._
      config.getConfig(configPath).entrySet().asScala.map(x => {
        val key = HostDistance.valueOf(x.getKey.substring(0,x.getKey.indexOf('.')))
        val value = x.getValue.unwrapped().asInstanceOf[Int]
        (key,value)
      }).toMap
    }

    def setHostDistanceIfExists(path:String, dest:(HostDistance, Int) => PoolingOptions):Unit = {
      if(config.hasPath(path)){
        getHostDistanceMap(path).foreach(x => dest(x._1,x._2))
      }else{
        //TODO some action/message that the path doesn't exist
      }
    }
    setHostDistanceIfExists("core-connections-per-host", po.setCoreConnectionsPerHost(_,_))
    setHostDistanceIfExists("max-connections-per-host", po.setMaxConnectionsPerHost(_,_))
    setHostDistanceIfExists("new-connection-threshold", po.setNewConnectionThreshold(_,_))
    setHostDistanceIfExists("max-requests-per-connection", po.setMaxRequestsPerConnection(_,_))
    setHostDistanceIfExists("max", po.setMaxRequestsPerConnection(_,_))

    po;
  }

  private [cassandra] def getQueryOptions(config: Config):QueryOptions = {
    implicit val conf: Config = config
    val qo = new QueryOptions()

    def setConsistencyLevelWithString(value:String) = {
    qo.setConsistencyLevel(ConsistencyLevel.valueOf(value))
    }
    def setSerialConsistencyLevelWithString(value:String) = {
      qo.setSerialConsistencyLevel(ConsistencyLevel.valueOf(value))
    }

    setIfExists[String,QueryOptions]("consistency-level", config.getString(_),  setConsistencyLevelWithString(_))
    setIfExists[Boolean,QueryOptions]("default-idempotence", config.getBoolean(_),  qo.setDefaultIdempotence(_))
    setIfExists[Int,QueryOptions]("fetch-size", config.getInt(_),  qo.setFetchSize(_))
    setIfExists[Boolean,QueryOptions]("metadata-enabled", config.getBoolean(_),  qo.setMetadataEnabled(_))
    setIfExists[Boolean,QueryOptions]("prepare-on-all-hosts", config.getBoolean(_),  qo.setPrepareOnAllHosts(_))
    setIfExists[Int,QueryOptions]("max-pending-refresh-node-list-requests", config.getInt(_),  qo.setMaxPendingRefreshNodeListRequests(_))
    setIfExists[Int,QueryOptions]("max-pending-refresh-node-Requests", config.getInt(_),  qo.setMaxPendingRefreshNodeRequests(_))
    setIfExists[Int,QueryOptions]("max-pending-refresh-schema-requests", config.getInt(_),  qo.setMaxPendingRefreshSchemaRequests(_))
    setIfExists[Int,QueryOptions]("refresh-node-interval-in-millis", config.getInt(_),  qo.setRefreshNodeIntervalMillis(_))
    setIfExists[Int,QueryOptions]("refresh-node-list-interval-in-millis", config.getInt(_),  qo.setRefreshNodeListIntervalMillis(_))
    setIfExists[Int,QueryOptions]("refresh-schema-interval-in-millis", config.getInt(_),  qo.setRefreshSchemaIntervalMillis(_))
    setIfExists[Boolean,QueryOptions]("reprepare-on-up", config.getBoolean(_),  qo.setReprepareOnUp(_))
    setIfExists[String,QueryOptions]("serial-consistency-level", config.getString(_),  setSerialConsistencyLevelWithString(_))

    qo
  }

  private [cassandra] def getAuthProvider(config:Config):Option[AuthProvider] = {
    implicit val conf = config
    val username = maybeGetValue[String]("plain-text-auth-provider.username", config.getString(_))
    val password = maybeGetValue[String]("plain-text-auth-provider.password", config.getString(_))
    if(username.isDefined && password.isDefined){
      Some(new PlainTextAuthProvider(username.get, password.get))
    }
    else{
      None
    }
  }

  private [cassandra] def getLoadBalancingPolicy(config:Config): Option[LoadBalancingPolicy] = {
    implicit val conf: Config = config

    getConfigIfHasPath("dc-aware-round-robin-policy").foreach(c => {
      val builder = DCAwareRoundRobinPolicy.builder()
      setIfExists("local-dc", c.getString(_), builder.withLocalDc(_))
      setIfExists("used-hosts-per-remote-dc", c.getInt(_), builder.withUsedHostsPerRemoteDc(_))
      executeIfExists("used-hosts-per-remote-dc", c.getBoolean(_), builder.allowRemoteDCsForLocalConsistencyLevel())
      return Some(builder.build())
    })

    getConfigIfHasPath("round-robin-policy").foreach(c => return Some(new RoundRobinPolicy()))

    getConfigIfHasPath("token-aware-policy").foreach(c => {
      val shuffleReplicas = maybeGetValue[Boolean]("shuffle-replicas", config.getBoolean(_))
      val childPolicyConfig = c.getConfig("child-policy")
      println(childPolicyConfig)
      val childPolicy = getLoadBalancingPolicy(childPolicyConfig)

      if(shuffleReplicas.isDefined){
        return Some(new TokenAwarePolicy(childPolicy.get, shuffleReplicas.get))
      }else{
        return Some(new TokenAwarePolicy(childPolicy.get))
      }
    })

    return None
  }

  private [cassandra] def getSocketOptions(config: Config):SocketOptions = {
    implicit val conf: Config = config
    val so = new SocketOptions()
    setIfExists[Int,SocketOptions]("connect-timeout-in-millis", config.getInt(_),  so.setConnectTimeoutMillis(_))
    setIfExists[Boolean,SocketOptions]("keep-alive", config.getBoolean(_),  so.setKeepAlive(_))
    setIfExists[Int,SocketOptions]("read-timeout-in-millis", config.getInt(_),  so.setReadTimeoutMillis(_))
    setIfExists[Int,SocketOptions]("receive-buffer-size", config.getInt(_),  so.setReceiveBufferSize(_))
    setIfExists[Boolean,SocketOptions]("reuse-address", config.getBoolean(_),  so.setReuseAddress(_))
    setIfExists[Int,SocketOptions]("send-buffer-size", config.getInt(_),  so.setSendBufferSize(_))
    setIfExists[Int,SocketOptions]("so-linger", config.getInt(_),  so.setSoLinger(_))
    setIfExists[Boolean,SocketOptions]("tcp-no-delay", config.getBoolean(_),  so.setTcpNoDelay(_))
    so
  }

  private [cassandra] def getSpeculativeExecutionPolicy(config: Config): Option[SpeculativeExecutionPolicy] = {
    implicit val cong:Config = config
    getConfigIfHasPath("no-speculative-execution-policy").foreach(c => {
      return Some(NoSpeculativeExecutionPolicy.INSTANCE)
    })
    getConfigIfHasPath("constant-speculative-execution-policy").foreach(c => {
      val constantDelayMillis = maybeGetValue("constant-delay-in-millis", config.getLong(_))
      val maxSpeculativeExecutions = maybeGetValue("max-speculative-executions", config.getInt(_))

      if (constantDelayMillis.isDefined && maxSpeculativeExecutions.isDefined)
        return Some(new ConstantSpeculativeExecutionPolicy(constantDelayMillis.get, maxSpeculativeExecutions.get))
    })
    getConfigIfHasPath("percentile-speculative-execution-policy").foreach(c => {

      val percentile = maybeGetValue("percentile", config.getDouble(_))
      val maxSpeculativeExecutions = maybeGetValue("max-speculative-executions", config.getInt(_))

      val percentileTrackerConfig = getConfigIfHasPath("percentile-tracker")
      if (percentileTrackerConfig.isDefined) {
        val percentileTrackerOption = getPercentileTrackerConfig(percentileTrackerConfig.get)
        if (percentileTrackerOption.isDefined && percentile.isDefined && maxSpeculativeExecutions.isDefined) {
          return Some(new PercentileSpeculativeExecutionPolicy(percentileTrackerOption.get, percentile.get, maxSpeculativeExecutions.get))
        }
      }
    })
    None
  }

  private [cassandra] def getPercentileTrackerConfig(config:Config): Option[PercentileTracker] ={

    implicit val conf:Config = config
    def getPercentileTrackerOption(percentileTrackerName : String): Option[(Long, Int, Int, Long)] ={
      getConfigIfHasPath(percentileTrackerName).foreach(c => {
        val highestTrackableLatencyMillis = maybeGetValue("highest-trackable-latency-in-millis", config.getLong(_))
        val numOfSigValueDigs = maybeGetValue("number-of-significant-value-digits", config.getInt(_))
        val minRecordedValues = maybeGetValue("min-recorded-values", config.getInt(_))
        val intervalMillis = maybeGetValue("interval-in-millis", config.getLong(_))

        if(highestTrackableLatencyMillis.isDefined && numOfSigValueDigs.isDefined && minRecordedValues.isDefined && intervalMillis.isDefined){
          return Some(new Tuple4[Long,Int,Int,Long](
            highestTrackableLatencyMillis.get,
            numOfSigValueDigs.get,
            minRecordedValues.get,
            intervalMillis.get
          ))
        }
      })
      None
    }

    getPercentileTrackerOption("cluster-wide-percentile-tracker").foreach(c => {
      return Some(ClusterWidePercentileTracker
        .builder(c._1)
        .withNumberOfSignificantValueDigits(c._2)
        .withMinRecordedValues(c._3)
        .withInterval(c._4, TimeUnit.MILLISECONDS)
        .build())
    })

    getPercentileTrackerOption("per-host-percentile-tracker").foreach(c => {
      return Some(PerHostPercentileTracker
        .builder(c._1)
        .withNumberOfSignificantValueDigits(c._2)
        .withMinRecordedValues(c._3)
        .withInterval(c._4, TimeUnit.MILLISECONDS)
        .build())
    })
    None
  }
}
