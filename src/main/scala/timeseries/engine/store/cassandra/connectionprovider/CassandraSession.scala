package timeseries.engine.store.cassandra.connectionprovider

import com.datastax.driver.core._
import com.google.common.util.concurrent.{FutureCallback, Futures, ListenableFuture, MoreExecutors}

import scala.concurrent.{Future, Promise}

class RichListenableFuture[T](lf: ListenableFuture[T]) {
  implicit def asScala: Future[T] = {
    val p = Promise[T]()
    Futures.addCallback(lf, new FutureCallback[T] {
      def onFailure(t: Throwable): Unit = p failure t
      def onSuccess(result: T): Unit    = p success result
    }, MoreExecutors.directExecutor)
    p.future
  }
}
object CassandraSession {

  def getCluster(config:CassandraConfig) : Cluster = {
    val b = Cluster.builder()
    b.addContactPointsWithPorts(config.contactPoints:_*)
    config.poolingOptions.map(b.withPoolingOptions(_))
    config.retryPolicy.map(b.withRetryPolicy(_))
    config.loadBalancingPolicy.map(b.withLoadBalancingPolicy(_))
    config.socketOptions.map(b.withSocketOptions(_))
    config.authProvider.map(b.withAuthProvider(_))
    config.queryOptions.map(b.withQueryOptions(_))
    config.speculativeExecutionPolicy.map(b.withSpeculativeExecutionPolicy(_))
    b.build()
  }

  def createSession(config:CassandraConfig) : Session = {
    val session = getCluster(config).connect()
    session
  }

  def asyncCreateSession(config:CassandraConfig) : Future[Session] = {
    getCluster(config).asyncConnect()
  }
}
