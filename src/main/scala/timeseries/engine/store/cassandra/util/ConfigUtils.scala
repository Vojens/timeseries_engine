package timeseries.engine.store.cassandra.util

import com.datastax.driver.core.policies._
import com.typesafe.config.Config

object ConfigUtils {
  private [cassandra] def maybeGet[T](path:String, getFunc : Config => T)(implicit config:Config):Option[T] = {
    if(config.hasPath(path)){
      Option[T](getFunc(config.getConfig(path)))
    }
    else{
      None
    }
  }
  private [cassandra] def maybeGet[T](path:String, getFunc : Config => T, default: T)(implicit config:Config):Option[T] = {
    if(config.hasPath(path)){
      Option[T](getFunc(config.getConfig(path)))
    }
    else{
      Option[T](default)
    }
  }
  private [cassandra] def maybeGetValue[T](path:String, getFunc : String => T)(implicit config:Config):Option[T] = {
    if(config.hasPath(path)){
      Option[T](getFunc(path))
    }
    else{
      None
    }
  }
  private [cassandra] def maybeGetValue[T,T1](path:String, getFunc : String => T, trans : T => T1)(implicit config:Config):Option[T1] = {
    if(config.hasPath(path)){
      Option[T1](trans(getFunc(path)))
    }
    else{
      None
    }
  }

  private [cassandra] def getConfigIfHasPath(path:String)(implicit config:Config):Option[Config] ={
    if(config.hasPath(path))
      Some(config.getConfig(path))
    else
      None
  }

  private [cassandra] def getRetryPolicy(config:Config): Option[RetryPolicy] = {
    implicit val conf: Config = config


    getConfigIfHasPath("default-retry-policy").foreach(c => {
      return Some(DefaultRetryPolicy.INSTANCE)
    })

    getConfigIfHasPath("downgrading-consistency-retry-policy").foreach(c => {
      return Some(DowngradingConsistencyRetryPolicy.INSTANCE)
    })

    getConfigIfHasPath("fallthrough-retry-policy").foreach(c => {
      return Some(FallthroughRetryPolicy.INSTANCE)
    })

    getConfigIfHasPath("logging-retry-policy").foreach(c => {
      val childPolicyConfig = c.getConfig("policy")
      return getRetryPolicy(childPolicyConfig) match {
        case Some(x) => Some(new LoggingRetryPolicy(x))
        case None => None
      }
    })
    return None
  }

  def setIfExists[T,R](path: String, source : String => T, dest : T => R)(implicit config:Config): Unit ={
    if(config.hasPath(path))
      dest(source(path))
  }

  def executeIfExists(path: String, source : String => Boolean, predicate: => Unit)(implicit config:Config): Unit ={
    if(config.hasPath(path) && source(path))
      predicate
  }

}
