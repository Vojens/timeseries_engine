package timeseries.engine.store.cassandra.util

import java.util.concurrent.TimeUnit

import com.codahale.metrics.Timer
import com.datastax.driver.core.{ResultSet, Row}
import timeseries.engine.Implicits._

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

class PrefetchingResultSetIterator(resultSet: ResultSet, prefetchWindowSize: Int, timer: Option[Timer] = None)(implicit ec: ExecutionContext) extends Iterator[Row] {

  private[this] val iterator = resultSet.iterator()

  override def hasNext = iterator.hasNext

  private[this] def maybePrefetch(): Unit = {
    if (!resultSet.isFullyFetched && resultSet.getAvailableWithoutFetching < prefetchWindowSize) {
      val t0 = System.nanoTime()
      val future = resultSet.fetchMoreResults().asScala
      if (timer.isDefined){
        future.onComplete{
          case Success(_) => timer.get.update(System.nanoTime() - t0, TimeUnit.NANOSECONDS)
          case Failure(f) => throw f
        }
      }
    }
  }

  override def next() = {
    maybePrefetch()
    iterator.next()
  }
}