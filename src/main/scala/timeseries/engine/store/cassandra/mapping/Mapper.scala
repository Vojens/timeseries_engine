package timeseries.engine.store.cassandra.mapping

import com.datastax.driver.core.Row

trait Mapper[T] {

  private [cassandra] def enumeration : Enumeration
  private [cassandra] def maybeMap(implicit row:Row) : Option[T]

  private [mapping] def maybeGet[V](name:String, func : String => V)(implicit row:Row) : Option[V] = {
    if(row.isNull(name)){
      None
    }else{
      Some(func(name))
    }
  }
}
