package timeseries.engine.store.cassandra.mapping

import java.util.UUID

import com.datastax.driver.core.Row

object StaticChannelMetadataMapper extends Mapper[timeseries.engine.dao.cassandra.StaticChannelMetadata] {
  val enumeration = StaticChannelMetadata

  override final def maybeMap(implicit row: Row) : Option[timeseries.engine.dao.cassandra.StaticChannelMetadata] = {
    if(row == null) return None

    val id = maybeGet[UUID](StaticChannelMetadata.id.toString , row.getUUID(_:String))
    if(id.isDefined){
      val `type` = maybeGet[java.lang.Byte](StaticChannelMetadata.`type`.toString , row.getByte(_:String))
      val externalType = maybeGet[java.lang.Byte](StaticChannelMetadata.externalType.toString , row.getByte(_:String))
      val annotation = maybeGet[java.lang.String](StaticChannelMetadata.annotation.toString , row.getString(_:String))
      val uri = maybeGet[java.lang.String](StaticChannelMetadata.uri.toString , row.getString(_:String))
      val dataType = maybeGet[java.lang.Byte](StaticChannelMetadata.dataType.toString , row.getByte(_:String))
      val dataSetType = maybeGet[java.lang.Byte](StaticChannelMetadata.dataSetType.toString , row.getByte(_:String))
      val customDataType = maybeGet[java.lang.String](StaticChannelMetadata.customDataType.toString , row.getString(_:String))
      val mnemonic = maybeGet[java.lang.String](StaticChannelMetadata.mnemonic.toString , row.getString(_:String))
      val uom = maybeGet[java.lang.String](StaticChannelMetadata.uom.toString , row.getString(_:String))
      val additionalIndices = maybeGet[java.util.List[Integer]](StaticChannelMetadata.additionalIndices.toString , row.getList(_, classOf[java.lang.Integer]))
      val extensions  = maybeGet[java.util.Map[java.lang.String,java.lang.String]](StaticChannelMetadata.extensions.toString , row.getMap(_, classOf[java.lang.String], classOf[java.lang.String]))
      val description = maybeGet[java.lang.String](StaticChannelMetadata.description.toString , row.getString(_:String))
      val timelineId = maybeGet[java.util.UUID](StaticChannelMetadata.timelineId.toString , row.getUUID(_:String))
      val sources = maybeGet[java.util.List[java.util.UUID]](StaticChannelMetadata.sources.toString , row.getList(_, classOf[java.util.UUID]))
      val bucketInfo = maybeGet[java.util.Map[java.lang.String,java.lang.String]](StaticChannelMetadata.bucketInfo.toString , row.getMap(_, classOf[java.lang.String], classOf[java.lang.String]))
      val indexType = maybeGet[java.lang.Byte](StaticChannelMetadata.indexType.toString , row.getByte(_:String))
      val indexUOM = maybeGet[java.lang.String](StaticChannelMetadata.indexUOM.toString , row.getString(_:String))
      val indexDTM = maybeGet[java.lang.String](StaticChannelMetadata.indexDTM.toString , row.getString(_:String))
      val indexDirection = maybeGet[java.lang.Byte](StaticChannelMetadata.indexDirection.toString , row.getByte(_:String))
      val indexMnemonic = maybeGet[java.lang.String](StaticChannelMetadata.indexMnemonic.toString , row.getString(_:String))
      val indexDescription = maybeGet[java.lang.String](StaticChannelMetadata.indexDescription.toString , row.getString(_:String))
      val indexUri = maybeGet[java.lang.String](StaticChannelMetadata.indexUri.toString , row.getString(_:String))
      val changeTime = maybeGet[java.util.Date](StaticChannelMetadata.changeTime.toString , row.getTimestamp((_:String)))
      val changeTime_Date = maybeGet[com.datastax.driver.core.LocalDate](StaticChannelMetadata.changeTime_Date.toString , row.getDate((_:String)))
      val changeTime_Time = maybeGet[java.lang.Long](StaticChannelMetadata.changeTime_Time.toString , row.getTime(_:String))
      val creationTime = maybeGet[java.util.Date](StaticChannelMetadata.creationTime.toString , row.getTimestamp((_:String)))
      val creationTime_Date = maybeGet[com.datastax.driver.core.LocalDate](StaticChannelMetadata.creationTime_Date.toString , row.getDate((_:String)))
      val creationTime_Time = maybeGet[java.lang.Long](StaticChannelMetadata.creationTime_Time.toString , row.getTime(_:String))
      val isDeleted = maybeGet[java.lang.Boolean](StaticChannelMetadata.isDeleted.toString , row.getBool(_:String))

      val staticChannelMetadata = timeseries.engine.dao.cassandra.StaticChannelMetadata(
        id.get,
        `type`,
        externalType,
        annotation,
        uri,
        dataType,
        dataSetType,
        customDataType,
        mnemonic,
        uom,
        additionalIndices,
        extensions,
        description,
        timelineId,
        sources,
        bucketInfo,
        indexType,
        indexUOM,
        indexDTM,
        indexDirection,
        indexMnemonic,
        indexDescription,
        indexUri,
        changeTime,
        changeTime_Date,
        changeTime_Time,
        creationTime,
        creationTime_Date,
        creationTime_Time,
        isDeleted
      )
      Some(staticChannelMetadata)
    }else{
      None
    }
  }


  private [cassandra] object StaticChannelMetadata extends Enumeration{

    val id = Value("id")
    val `type` = Value("typ")
    val externalType = Value("exttyp")
    val annotation = Value("annt")
    val uri = Value("uri")
    val dataType = Value("dattyp")
    val dataSetType = Value("datsettyp")
    val customDataType = Value("custdattyp")
    val mnemonic = Value("mnem")
    val uom = Value("uom")
    val additionalIndices = Value("addlidx")
    val extensions = Value("extn")
    val description = Value("descr")
    val timelineId = Value("timlinid")
    val sources = Value("src")
    val bucketInfo = Value("bktinfo")
    val indexType = Value("idxtyp")
    val indexUOM = Value("idxuom")
    val indexDTM = Value("idxdtm")
    val indexDirection = Value("idxdircn")
    val indexMnemonic = Value("idxmnem")
    val indexDescription = Value("idxdescr")
    val indexUri = Value("idxuri")
    val changeTime = Value("ctime")
    val changeTime_Date = Value("ctime_d")
    val changeTime_Time = Value("ctime_t")
    val creationTime = Value("crtime")
    val creationTime_Date = Value("crtime_d")
    val creationTime_Time = Value("crtime_t")
    val isDeleted = Value("isdel")
  }
}
