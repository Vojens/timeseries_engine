package timeseries.engine.store.cassandra.mapping
import com.datastax.driver.core.Row
import timeseries.engine.dao.cassandra.ChannelBucketTimeline

object ChannelBucketTimelineMapper extends Mapper[ChannelBucketTimeline] with Cql3DataTypes {
  override private [cassandra] def enumeration = ChannelBucketTimeline

  private [cassandra] object ChannelBucketTimeline extends Enumeration {
    val chid = Value("chid")
    val timlinid = Value("timlinid")
    val bkt = Value("bkt")
    val bktid = Value("bktid")
  }

  override private[cassandra] def maybeMap(implicit row: Row) : Option[ChannelBucketTimeline] = {
    if(row == null) return None

    val chid = maybeGet[uuid](ChannelBucketTimeline.chid.toString , row.getUUID(_:String))
    val timlinid = maybeGet[uuid](ChannelBucketTimeline.timlinid.toString , row.getUUID(_:String))
    val bkt = maybeGet[blob](ChannelBucketTimeline.bkt.toString , row.getBytes(_:String))

    if(chid.isDefined && timlinid.isDefined && bkt.isDefined){
      val bktid = maybeGet[uuid](ChannelBucketTimeline.bktid.toString , row.getUUID(_:String))
      val channelBucketTimeline = timeseries.engine.dao.cassandra.ChannelBucketTimeline(
        chid.get,
        timlinid.get,
        bkt.get,
        bktid)
      Some(channelBucketTimeline)
    }else{
      None
    }
  }
}
