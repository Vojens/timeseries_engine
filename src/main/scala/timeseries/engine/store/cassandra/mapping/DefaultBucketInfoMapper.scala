package timeseries.engine.store.cassandra.mapping

import com.datastax.driver.core.Row
import timeseries.engine.dao.cassandra.DefaultBucketInfo

object DefaultBucketInfoMapper  extends Mapper[DefaultBucketInfo] with Cql3DataTypes {
  override private [cassandra] def enumeration = DefaultBucketInfo

  override private[cassandra] def maybeMap(implicit row: Row) : Option[DefaultBucketInfo] = {
    if(row == null) return None

    val typ = maybeGet[tinyint](DefaultBucketInfo.typ.toString , row.getByte(_:String))
    val dattyp = maybeGet[tinyint](DefaultBucketInfo.dattyp.toString , row.getByte(_:String))

    if(typ.isDefined && dattyp.isDefined){
      val bktinfo = maybeGet[map[ascii,ascii]](DefaultBucketInfo.bktinfo.toString , row.getMap(_, classOf[ascii], classOf[ascii]))
      val dbi = timeseries.engine.dao.cassandra.DefaultBucketInfo(
        typ.get,
        dattyp.get,
        bktinfo)
      Some(dbi)
    }else{
      None
    }

  }

  private [cassandra] object DefaultBucketInfo extends Enumeration{
    val typ = Value("typ")
    val dattyp = Value("dattyp")
    val bktinfo = Value("bktinfo")
  }
}
