package timeseries.engine.store.cassandra.mapping

trait Cql3DataTypes {
  type ascii = java.lang.String
  type bigint = java.lang.Long
  type blob = java.nio.ByteBuffer
  type boolean = java.lang.Boolean
  type counter = java.lang.Long
  type date = com.datastax.driver.core.LocalDate
  type decimal = java.math.BigDecimal
  type double = java.lang.Double
  type float = java.lang.Float
  type inet = java.net.InetAddress
  type int = java.lang.Integer
  type list[_] = java.util.List[_]
  type map[A,B] = java.util.Map[A,B]
  type set[_] = java.util.Set[_]
  type smallint = java.lang.Short
  type text = java.lang.String
  type time = java.lang.Long
  type timestamp = java.util.Date
  type timeuuid = java.util.UUID
  type tinyint = java.lang.Byte
  type tuple = com.datastax.driver.core.TupleType
  type udf = com.datastax.driver.core.UserType
  type uuid = java.util.UUID
  type varchar = java.lang.String
  type varint = java.math.BigInteger
}
