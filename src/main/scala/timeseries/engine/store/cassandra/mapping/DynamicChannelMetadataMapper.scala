package timeseries.engine.store.cassandra.mapping

import com.datastax.driver.core.Row
import java.util.UUID


object DynamicChannelMetadataMapper extends Mapper[timeseries.engine.dao.cassandra.DynamicChannelMetadata]{
  
  val enumeration = DynamicChannelMetadata
  
  override def maybeMap(implicit row: Row) : Option[timeseries.engine.dao.cassandra.DynamicChannelMetadata] = {
    if(row == null) return None

    val id = maybeGet[UUID](DynamicChannelMetadata.id.toString , row.getUUID(_:String))
    if(id.isDefined){
      val subscript_i=maybeGet[java.lang.Byte](DynamicChannelMetadata.subscript_i.toString, row.getByte(_:String))
      val subscript_j=maybeGet[java.lang.Byte](DynamicChannelMetadata.subscript_j.toString,row.getByte(_:String))
      val element=maybeGet[java.nio.ByteBuffer](DynamicChannelMetadata.element.toString, row.getBytes(_:String))
      val tags=maybeGet[java.util.Map[java.lang.String,java.lang.String]](DynamicChannelMetadata.tags.toString, row.getMap(_, classOf[java.lang.String], classOf[java.lang.String]))
      val dynamicChannelMetaData= timeseries.engine.dao.cassandra.DynamicChannelMetadata(
            id.get,
            subscript_i.get,
            subscript_j.get,
            element,
            tags
          )
      Some(dynamicChannelMetaData)
    }else{
      None
    }
  }
  
  private [cassandra] object DynamicChannelMetadata extends Enumeration{
    val id = Value("id")
    val subscript_i = Value("subscript_i")
    val subscript_j = Value("subscript_j")
    val element = Value("element")
    val tags = Value("tags")
  }
  
}