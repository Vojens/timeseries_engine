package timeseries.engine.module

case class ResponseObj(
  val status:String,
  val message:String
)