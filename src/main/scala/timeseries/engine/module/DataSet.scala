package timeseries.engine.module

import java.util.UUID

sealed abstract class DataSet {
def id:UUID;
  def toDataSetEntryStream : Stream[DataSetEntry]
}

case class TimestampDataSet[V <: Value](id:UUID, val series: TimestampSeries[V]) extends DataSet{
  override def toDataSetEntryStream = series.map.toStream.map(x => DataSetEntry(id, DataPoint(x._1,x._2)))
}


case class DoubleDataSet[V <: Value](id:UUID, val series: DoubleSeries[V]) extends DataSet{
  override def toDataSetEntryStream = series.map.toStream.map(x => DataSetEntry(id, DataPoint(x._1,x._2)))
}

case class DataSetEntry(id : UUID, dataPoint: DataPoint)