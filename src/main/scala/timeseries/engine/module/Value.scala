package timeseries.engine.module

import com.google.common.base.CharMatcher

//@see https://avro.apache.org/docs/1.8.0/spec.html#Logical+Types
sealed trait Value;

case class AsciiValue(val value: String) extends Value{ //asciiValue:       Option[java.lang.String],     //ascii ASCII
  require(CharMatcher.ascii().matchesAllOf(value), "value must be ascii")
}
case class BigIntValue(val value: Long) extends Value  //longValue:        Option[java.lang.Long],       //bi 	  bigint
case class BlobValue(val value: Array[Byte]) extends Value //blobValue:        Option[java.nio.ByteBuffer],  //blb 	BLOB
case class DoubleValue(val value: Double) extends Value  //doubleValue:      Option[java.lang.Double],     //d	    double
case class DecimalValue(val value: java.math.BigDecimal) extends Value  //decimalValue:     Option[java.math.BigDecimal], //dec	  DECIMAL
case class IntValue(val value: Int) extends Value  //intValue:         Option[java.lang.Integer],    //i	    int
case class InetAddressValue(val value: java.net.InetAddress) extends Value  //iNetAddressValue: Option[java.net.InetAddress], //inet  inet
case class ShortValue(val value: Short) extends Value  //smallIntValue:    Option[java.lang.Short],      //si	  smallint
case class ByteValue(val value: Byte) extends Value  //tinyIntValue:     Option[java.lang.Byte],       //ti	  TINYINT

case class DateValue(val value: java.time.LocalDate) extends Value  //dateValue:        Option[com.datastax.driver.core.LocalDate],       //dt	  DATE

//this is time with nano seconds
case class TimeNanosValue(val value: java.time.LocalTime) extends Value  //timeValue:        Option[java.lang.Long],       //tim	  TIME
case class TimeMicrosValue(val value: java.time.LocalTime) extends Value
case class TimeMillisValue(val value: java.time.LocalTime) extends Value

//timestamps with different precisions
case class TimestampNanosValue(val value: java.time.LocalDateTime) extends Value
case class TimestampMicrosValue(val value: java.time.LocalDateTime) extends Value //timestampValue:   Option[java.util.Date],       //ts	  TIMESTAMP
case class TimestampMillisValue(val value: java.time.LocalDateTime) extends Value

case class Duration(val value : java.time.Duration) extends Value

case class TextValue(val value: String) extends Value  //textValue:        Option[java.lang.String],     //txt 	text
case class UuidValue(val value: java.util.UUID) extends Value  //uuidValue:        Option[java.util.UUID],       //vi	  varint
case class VarIntValue(val value: java.math.BigInteger) extends Value  //varIntValue:      Option[java.math.BigInteger], //uid	  UUID


object Value{
  implicit def stringToTextValue(s:String) = new TextValue(s)
}