package timeseries.engine.module

import scala.collection.immutable.Map


case class ChannelStaticMetadataRespObj(
  id:String, //to be parsed to uuid
  typ:Int,
  extTyp:Int,
  annt:String,
  uri:String,
  dataType:Int,
  dataSetType:Int,
  custDataType:String,
  mnemonic:String,
  uom:String,
  addlidx:Array[Int],
  extn:Map[String,String],
  descr:String,
  src:Array[String], //to be parsed to uuid list
  bucketInfo:Map[String,String],
  indexInfo:MetaDataIndexInfo,
  modificationTime:String,//to be parsed to date in format yyyy-MM-dd'T'HH:mm:ss.SSSZZ
  changeTime:String, //to be parsed to date in format yyyy-MM-dd'T'HH:mm:ss.SSSZZ
  creationTime:String, //to be parsed to date in format yyyy-MM-dd'T'HH:mm:ss.SSSZZ
  status:Int
)
