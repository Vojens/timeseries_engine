package timeseries.engine.module

case class GetLatest(
  val id:String,
  val limit:Int
)