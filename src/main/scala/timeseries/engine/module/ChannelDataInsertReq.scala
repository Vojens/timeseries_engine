package timeseries.engine.module

import scala.collection.immutable.Map


case class SeriesDataHeader(
  id:String,
  dataType:Int,
  `type`:Int
)

case class ChannelDataInsertReq(
  header:SeriesDataHeader,
  data:Array[Map[String,String]]
)
