package timeseries.engine.module

case class GetRange(
  val id:String,
  val startRange:String,
  val endRange:String
)
