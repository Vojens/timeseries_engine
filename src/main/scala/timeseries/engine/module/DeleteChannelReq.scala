package timeseries.engine.module

case class DeleteChannelReq(
  id:String,
  deleteReason:String,
  userId:String
)