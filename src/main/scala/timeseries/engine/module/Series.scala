package timeseries.engine.module

import timeseries.engine.common.data.{DoubleIndex, Index, TimestampIndex}

import scala.collection.immutable.SortedMap

sealed trait Series;
sealed trait SortedSeries extends Series;

sealed abstract class BaseSeries[I <: Index, V <: Value] extends Series{
  def map:Map[I, V]
}
sealed abstract class BaseTimestampSeries[V <: Value] extends BaseSeries[TimestampIndex, V] with Series;
sealed abstract class BaseDoubleSeries[V <: Value] extends BaseSeries[DoubleIndex, V] with Series;

case class TimestampSeries[V <: Value](val map: Map[TimestampIndex, V]) extends BaseTimestampSeries[V] with Series;
case class DoubleSeries[V <: Value](val map: Map[DoubleIndex, V]) extends BaseDoubleSeries[V] with Series;

case class SortedTimestampSeries[V <: Value](val map: SortedMap[TimestampIndex, V]) extends BaseTimestampSeries[V] with SortedSeries;
case class SortedDoubleSeries[V <: Value](val map: SortedMap[DoubleIndex, V]) extends BaseDoubleSeries[V] with SortedSeries;


object SortedDoubleSeries{
  def apply[V <: Value] (kvs: (DoubleIndex, V)*)(implicit ord: Ordering[DoubleIndex]) = scala.collection.immutable.TreeMap[DoubleIndex,V](kvs :_*)
}

object DoubleSeries{
  def apply[V <: Value] (kvs: (DoubleIndex, V)*) = scala.collection.immutable.Map[DoubleIndex,V](kvs :_*)
}

object SortedTimestampSeries{
  def apply[V <: Value] (kvs: (TimestampIndex, V)*)(implicit ord: Ordering[TimestampIndex]) = scala.collection.immutable.TreeMap[TimestampIndex,V](kvs :_*)
}

object TimestampSeries{
  def apply[V <: Value] (kvs: (TimestampIndex, V)*) = scala.collection.immutable.Map[TimestampIndex,V](kvs :_*)
}