package timeseries.engine.module

import timeseries.engine.common.data.Index

case class DataPoint (val index:Index, val value:Value);

object DataPoint{
  implicit def dataPointToTuple2(dp: DataPoint):Tuple2[Index,Value] = dp.index -> dp.value
}
