package timeseries.engine.module

case class DeleteChannelDataReq(
  id:String,
  startRange:String,
  endRange:String,
  deleteReason:String,
  userId:String
)