package timeseries.engine.module

case class MetaDataIndexInfo(
  idxType:Int,
  idxUOM:String,
  idxDTM:String,
  idxDircn:Int,
  idxMnemonic:String,
  idxDescr:String,
  idxUri:String
)