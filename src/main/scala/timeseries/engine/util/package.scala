package timeseries.engine

import java.nio.ByteBuffer

import com.datastax.driver.core.utils.Bytes
import com.google.common.primitives.Longs
import com.google.common.util.concurrent.{FutureCallback, Futures, ListenableFuture, MoreExecutors}

import scala.concurrent.{Future, Promise}

package object Implicits {
  implicit class RichListenableFuture[T](lf: ListenableFuture[T]) {
    def asScala: Future[T] = {
      val p = Promise[T]()
      Futures.addCallback(lf, new FutureCallback[T] {
        def onFailure(t: Throwable): Unit = p failure t
        def onSuccess(result: T): Unit    = p success result
      }, MoreExecutors.directExecutor)
      p.future
    }
  }

  implicit class RichByteBuffer(val self : ByteBuffer){
    def toByte = {
      val a = Bytes.getArray(self)
      val b: Byte = a(0)
      b
    }
    def toLong = {
      Longs.fromByteArray(self.array())
    }
    def toDouble = {
      ByteBuffer.wrap(self.array()).getDouble()
    }
  }

  implicit class RichByte(val self : Byte){
    def toByteBuffer = {
      val a = Array[Byte](self)
      val bb = ByteBuffer.wrap(a)
      bb
    }
  }

  implicit class RichLong(val self: Long){
    def toByteBuffer = {
      ByteBuffer.wrap(Longs.toByteArray(self))
    }
  }

  implicit class RichDouble(val self: Double){
    def toByteBuffer = {
      ByteBuffer.allocate(java.lang.Double.SIZE).putDouble(self)
    }
  }
}
