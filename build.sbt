
name := "TimeSeries_Engine"

version := "0.1"

scalacOptions ++= Seq(
  "-deprecation",
  "-unchecked",
  "-Xlint",
  "-Ywarn-unused",
  "-Ywarn-dead-code",
  "-feature",
  "-language:_"
)

enablePlugins(JavaAppPackaging)

scriptClasspath +="../conf"

libraryDependencies ++= {
  val scalaVersion = "2.12.4"
  val akkaVersion = "2.5.8"
  val akkaHttpVersion = "10.1.0-RC1"
  val logBackVersion = "1.2.3"
  val scalaVersionTest = "3.0.4"
  val cassandraDriverVersion = "3.3.2"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-remote" % akkaVersion,
    "com.typesafe.akka" %% "akka-http-core" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-parsing" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-slf4j"      % akkaVersion,
    "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion,
    "com.typesafe.akka" %% "akka-testkit"    % akkaVersion   % Test,
    "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % Test,
    "org.scalatest" %% "scalatest" % scalaVersionTest % Test,
    "org.scala-lang" % "scala-reflect" % scalaVersion,
    "net.jpountz.lz4" % "lz4" % "1.3.0",
    "joda-time" % "joda-time" % "2.9.7",
    "org.apache.avro" % "avro" % "1.8.1",
    "ch.qos.logback" % "logback-classic" % logBackVersion % Test,
    "com.google.guava" % "guava" % "23.5-jre",
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
    "com.datastax.cassandra" % "cassandra-driver-core" % cassandraDriverVersion,
    "com.datastax.cassandra" % "cassandra-driver-mapping" % cassandraDriverVersion,
    "com.datastax.cassandra" % "cassandra-driver-extras" % cassandraDriverVersion
  )
}